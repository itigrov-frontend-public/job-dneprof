declare module '*.css' {
	type IClassNames = Record<string, string>;
	const classnames: IClassNames;
	export = classnames;
}

declare module '*.scss' {
	type IClassNames = Record<string, string>;
	const classnames: IClassNames;
	export = classnames;
}

declare module '*.sass' {
	type IClassNames = Record<string, string>;
	const classnames: IClassNames;
	export = classnames;
}

declare module '*.png';
declare module '*.jpg';
declare module '*.jpeg';
declare module '*.svg' {
	import type React from 'react';
	// const SVG: React.VFC<React.SVGProps<SVGSVGElement>>;
	const SVG: React.FC<React.SVGProps<SVGSVGElement>>;
	export default SVG;
}

// eslint-disable-next-line @typescript-eslint/naming-convention
declare const __IS_DEV__: boolean;
// eslint-disable-next-line @typescript-eslint/naming-convention
declare const __IS_APP_TYPE__: 'frontend' | 'storybook' | 'jest';
