import path from 'path';
import type webpack from 'webpack';
import { type RuleSetRule } from 'webpack';
import { DefinePlugin } from 'webpack';

import { buildCssLoader } from '../build/loaders/buildCssLoader';
import { type BuildPaths } from '../build/types/config';

export default ({ config }: { config: webpack.Configuration }) => {
	const paths: BuildPaths = {
		build: '',
		html: '',
		entry: '',
		src: path.resolve(__dirname, '..', '..', 'src'),
		static: '', // for "copy-webpack-plugin" - copy static images
		locales: '' // for "copy-webpack-plugin" - copy locales
	};

	config!.resolve!.modules!.push(paths.src);

	// fixed BUG
	config!.resolve!.alias = {
		entities: path.resolve(__dirname, '..', '..', 'src', 'entities')
	};

	config!.resolve!.extensions!.push('.ts', '.tsx');
	// eslint-disable-next-line @typescript-eslint/ban-ts-comment
	// @ts-expect-error
	config!.module!.rules = config!.module!.rules!.map((rule: RuleSetRule) => {
		if (/svg/.test(rule.test as string)) {
			return { ...rule, exclude: /\.svg$/i };
		}
		return rule;
	});

	config!.module!.rules.push({
		test: /\.svg$/,
		use: ['@svgr/webpack']
	});

	config!.module!.rules.push(buildCssLoader(true));

	config!.plugins!.push(new DefinePlugin({
		__IS_DEV__: true,
		__IS_APP_TYPE__: JSON.stringify('storybook')
	}));

	return config;
};
