import { type StoryFn } from '@storybook/react';
import BreakpointsProvider from 'app/providers/BreakpointsProvider';

export const BreakpointsDecorator = (StoryComponent: StoryFn) => (
	<BreakpointsProvider>
		<StoryComponent />
	</BreakpointsProvider>
);
