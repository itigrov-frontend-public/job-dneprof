/* eslint-disable react/display-name */
import { Provider } from 'react-redux';
import { type StoryFn } from '@storybook/react';
import store from 'app/providers/AppStore';

export const StoreDecorator = (
	StoryComponent: StoryFn
) => (
	<Provider store={store}>
		<StoryComponent />
	</Provider>
);
