import { Suspense, useEffect } from 'react';
import { I18nextProvider } from 'react-i18next';
import { type StoryContext, type StoryFn } from '@storybook/react';
import i18nForTests from 'app/components/i18n/i18nForTests';
import { i18nNames } from 'shared/config/types/i18n';

export const I18nDecorator = (
	StoryComponent: StoryFn,
	context: StoryContext,
	importantLang?: i18nNames | undefined
) => {
	let { locale } = context.globals;

	if (importantLang) {
		locale = importantLang;
	}

	useEffect(() => {
		void i18nForTests.changeLanguage(locale || i18nNames.ru);
	}, [locale]);

	return (
		<Suspense fallback={<div>loading i18n...</div>}>
			<I18nextProvider i18n={i18nForTests}>
				<StoryComponent />
			</I18nextProvider>
		</Suspense>
	)
};
