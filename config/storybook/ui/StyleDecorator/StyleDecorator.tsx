import { type StoryFn } from '@storybook/react';

import 'shared/config/styles/index.scss';

export const StyleDecorator = (StoryComponent: StoryFn) => (<StoryComponent />);
