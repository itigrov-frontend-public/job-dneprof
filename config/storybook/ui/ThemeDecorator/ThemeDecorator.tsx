/* eslint-disable react/display-name */
import { Suspense } from 'react';
import { type StoryFn } from '@storybook/react';
import ThemeProvider from 'app/providers/ThemeProvider';
import { type themeNames } from 'shared/config/types/Theme';

export const ThemeDecorator = (
	StoryComponent: StoryFn
) => (theme?: themeNames) => {
	return (
		<ThemeProvider theme={theme}>
			<Suspense fallback="">
				<StoryComponent />
			</Suspense>
		</ThemeProvider>
	);
}
