import { type StoryContext, type StoryFn } from '@storybook/react';
import { setThemeHelper } from 'entities/Theme';
import { themeNames } from 'shared/config/types/Theme';

export const ThemeDecoratorStory = (
	StoryComponent: StoryFn,
	context: StoryContext,
	importantTheme?: themeNames | undefined
) => {
	let theme = context.globals.theme || themeNames.LIGHT;

	if (importantTheme) {
		theme = importantTheme;
	}

	setThemeHelper(theme)

	return (
		<StoryComponent />
	);
}
