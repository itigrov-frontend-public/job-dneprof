import { type Preview } from '@storybook/react';
import { withRouter } from 'storybook-addon-react-router-v6';

import { BreakpointsDecorator } from './ui/BreakpointsDecorator/BreakpointsDecorator';
import { StoreDecorator } from './ui/StoreDecorator/StoreDecorator';
import { StyleDecorator } from './ui/StyleDecorator/StyleDecorator';
import { SuspenseDecorator } from './ui/SuspensDecorator/SuspensDecorator';

const preview: Preview = {
	parameters: {
		actions: { argTypesRegex: '^on[A-Z].*' },
		controls: {
			matchers: {
				color: /(background|color)$/i,
				date: /Date$/
			}
		}
	},

	decorators: [withRouter]
	// decorators: []
};

preview!.decorators!.push((Story) => SuspenseDecorator(Story));
preview!.decorators!.push((Story) => BreakpointsDecorator(Story));
preview!.decorators!.push((Story) => StoreDecorator(Story));
// preview!.decorators!.push(withRouter);
// preview!.decorators!.push((Story) => RouterDecorator(Story));
preview!.decorators!.push((Story) => StyleDecorator(Story));
// preview.decorators.push((Story) => ThemeDecorator(Theme.LIGHT)(Story));

export default preview;

export const globalTypes = {
	locale: {
		// name: 'Locale',
		description: 'Язык',
		defaultValue: 'ru',
		toolbar: {
			title: 'Lang',
			icon: 'globe',
			items: [
				{ value: 'ru', title: 'Русский' },
				{ value: 'en', title: 'English' }
			],
			dynamicTitle: true,
			showName: true
		}
	}, // locale
	theme: {
		description: 'Тема',
		defaultValue: 'light',
		toolbar: {
			title: 'Theme',
			icon: 'mirror',
			items: [
				{ value: 'light', title: 'Светлая' },
				{ value: 'dark', title: 'Темная' }

			], // 'light', 'dark'
			dynamicTitle: true
		}
	} // theme

};
