import type { ReactNode } from 'react';
import { Provider } from 'react-redux';
import store from 'app/providers/AppStore';

export default function StoreDecorator ({ children }: { children: ReactNode }) {
	return <Provider store={store}>{children}</Provider>;
}
