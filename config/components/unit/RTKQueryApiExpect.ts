import { type UseQueryHookResult } from '@reduxjs/toolkit/dist/query/react/buildHooks';
import { waitFor } from '@testing-library/react';
import { type FetchMock } from 'jest-fetch-mock';

interface IRTKQueryApiExpect {
	result: { current: UseQueryHookResult<any> },
	fetchMock: FetchMock,
	endpointName: string,
}

export default async function RTKQueryApiExpect ({
	result,
	fetchMock,
	endpointName
}: IRTKQueryApiExpect): Promise<void> {
	expect(result.current).toMatchObject({
		status: 'pending',
		endpointName,
		isLoading: true,
		isSuccess: false,
		isError: false,
		isFetching: true
	});

	await waitFor(() => { expect(result.current.isSuccess).toBe(true); });
	expect(fetchMock).toBeCalledTimes(1);

	expect(result.current).toMatchObject({
		status: 'fulfilled',
		endpointName,
		data: {},
		isLoading: false,
		isSuccess: true,
		isError: false,
		currentData: {},
		isFetching: false
	});
}
