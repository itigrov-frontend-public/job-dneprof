import { type RuleSetRule } from 'webpack';

import { buildCssLoader } from './loaders/buildCssLoader';
import type { BuildOptions } from './types/config';

export function buildLoaders (options: BuildOptions): RuleSetRule[] {
	const svgLoader = {
		test: /\.svg$/i,
		use: ['@svgr/webpack']
	}

	const fileLoader = {
		test: /\.(png|jpe?g|gif|woff2|woff)$/i,
		use: ['file-loader']
	}

	const babelLoader = {
		test: /\.(js|jsx|tsx)$/,
		exclude: /node_modules/,
		use: {
			loader: 'babel-loader',
			options: {
				presets: ['@babel/preset-env']
			}
		}
	};

	const typescriptLoader = {
		test: /\.tsx?$/,
		use: 'ts-loader',
		exclude: /node_modules/
	}

	// ! Очередность лоадеров имеет значение
	return [
		svgLoader,
		fileLoader,
		babelLoader,
		typescriptLoader,
		buildCssLoader(options.isDev)
	];
}
