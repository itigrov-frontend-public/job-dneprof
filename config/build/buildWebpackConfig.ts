import type webpack from 'webpack';

import type { BuildOptions } from './types/config';
import { buildDevServer } from './buildDevServer';
import { buildLoaders } from './buildLoaders';
import { buildPlugins } from './buildPlugins';
import { buildResolves } from './buildResolves';

export function buildWebpackConfig (options: BuildOptions): webpack.Configuration {
	return {
		mode: options.mode, // "development" | "production" | "none"
		entry: {
			bundle: options.paths.entry
		},
		output: {
			filename: '[name].[contenthash].js', // filename: 'bundle.js'
			path: options.paths.build,
			publicPath: '/',
			clean: true // Очищать перед созданием
		},
		plugins: buildPlugins(options),
		module: {
			rules: buildLoaders(options)
		},
		resolve: buildResolves(options),
		devtool: options.isDev ? 'inline-source-map' : undefined,
		devServer: options.isDev ? buildDevServer(options) : undefined
	}
};
