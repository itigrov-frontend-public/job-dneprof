import ReactRefreshWebpackPlugin from '@pmmmwh/react-refresh-webpack-plugin';
import CopyPlugin from 'copy-webpack-plugin';
import HTMLWebpackPlugin from 'html-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import RemovePlugin from 'remove-files-webpack-plugin';
import webpack from 'webpack';
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';

import type { BuildOptions } from './types/config';

export function buildPlugins (options: BuildOptions): webpack.WebpackPluginInstance[] {
	const plugins = [
		new HTMLWebpackPlugin({
			template: options.paths.html
		}), // Создает index.html
		new webpack.ProgressPlugin(),
		new MiniCssExtractPlugin({
			filename: 'css/[name].[contenthash:8].css',
			chunkFilename: 'css/[name].[contenthash:8].css'
		}),

		// Передача параметров WebPack в само приложение (например для логирования i18n в DEV режиме)
		new webpack.DefinePlugin({
			__IS_DEV__: JSON.stringify(options.isDev),
			__IS_APP_TYPE__: JSON.stringify('frontend')
		})
	]

	if (options.isDev) {
		plugins.push(new webpack.HotModuleReplacementPlugin());
		plugins.push(new ReactRefreshWebpackPlugin());
	}

	plugins.push(new CopyPlugin({
		patterns: [
			{ from: options.paths.static }
		]
	}))
	plugins.push(new CopyPlugin({
		patterns: [
			{
				from: options.paths.locales,
				to: 'locales'
			}
		]
	}))

	// DELETE build folder on: npm run build
	plugins.push(new RemovePlugin({
		before: {
			include: [
				options.paths.build
			]
		},
		watch: {},
		after: {}
	}))

	if (options.analyze) {
		plugins.push(new BundleAnalyzerPlugin({}));
	}

	return plugins;
}
