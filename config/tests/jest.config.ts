import path from 'path';

export default {
	notify: true,
	globals: {
		__IS_DEV__: true,
		__IS_APP_TYPE__: 'jest'
	},
	clearMocks: true,
	testEnvironment: 'jsdom',
	coveragePathIgnorePatterns: [
		'\\\\node_modules\\\\'
	],
	moduleFileExtensions: [
		'js',
		'jsx',
		'ts',
		'tsx',
		'json',
		'node'
	],
	moduleDirectories: [
		'node_modules'
	],
	modulePaths: [
		'<rootDir>src'
	],
	testMatch: [
		// ! Обнаружил разницу между МАК ОС и ВИНДОУС!!!
		'<rootDir>src/**/*(*.)@(spec|test).[tj]s?(x)'
	],
	rootDir: '../../',
	setupFilesAfterEnv: ['<rootDir>config/tests/setupTests.ts'],
	moduleNameMapper: {
		'entities/(.*)': '<rootDir>src/entities/$1',
		// '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':'<rootDir>/__mocks__/fileMock.js',
		'\\.s?css$': 'identity-obj-proxy',
		'\\.svg': path.resolve(__dirname, '..', 'tests', 'components', 'jestEmptyComponent.tsx')
	}
};
