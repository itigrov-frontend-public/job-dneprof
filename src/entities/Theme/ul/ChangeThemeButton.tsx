import { type FC, type HTMLAttributes, memo, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { themeNames } from 'shared/config/types/Theme';
import SvgMoon from 'shared/static/svg/icons/moon.svg'
import SvgSun from 'shared/static/svg/icons/sun.svg'
import { Icon } from 'shared/ui/Icon';

import { getTheme } from '../selectors/getTheme';
import { themeSliceActions } from '../store/themeSlice';

interface ChangeThemeButtonProps extends HTMLAttributes<HTMLElement> {}

const ChangeThemeButton: FC<ChangeThemeButtonProps> = (props) => {
	const {
		title,
		...otherProps
	} = props
	const { t } = useTranslation();

	const theme = useSelector(getTheme)
	const dispatch = useDispatch();

	const toggleTheme = useCallback(() => {
		dispatch(themeSliceActions.toggleTheme());
	}, [dispatch]);

	return (
		<Icon
			tag='button'
			hover
			title={title || t('button-ChangeThemeButton')}
			onClick={ toggleTheme }
			{...otherProps}
		>{theme === themeNames.LIGHT ? <SvgMoon/> : <SvgSun/> }</Icon>
	)
}

export default memo(ChangeThemeButton)
