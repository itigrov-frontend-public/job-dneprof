import { type StateSchema } from 'shared/config/store/AppStore';

export const getTheme = (state: StateSchema) => state.theme?.theme;
