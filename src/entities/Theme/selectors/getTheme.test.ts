import { type DeepPartial } from '@reduxjs/toolkit';
import { type StateSchema } from 'shared/config/store/AppStore';
import { themeNames } from 'shared/config/types/Theme';

import { getTheme } from './getTheme';

describe('getTheme', () => {
	test('getTheme: LIGHT', () => {
		const state: DeepPartial<StateSchema> = {
			theme: { theme: themeNames.LIGHT }
		};
		expect(getTheme(state as StateSchema)).toBe(themeNames.LIGHT);
	});

	test('getTheme: DARK', () => {
		const state: DeepPartial<StateSchema> = {
			theme: { theme: themeNames.DARK }
		};
		expect(getTheme(state as StateSchema)).toBe(themeNames.DARK);
	});

	test('getTheme: undefined', () => {
		const state: DeepPartial<StateSchema> = { };
		expect(getTheme(state as StateSchema)).toBe(undefined);
	});
});
