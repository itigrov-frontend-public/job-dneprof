import { createSlice, type PayloadAction } from '@reduxjs/toolkit';
import { appStorage, appStorageList } from 'shared/components/appStorage';
import { type IThemeSlice, themeNames } from 'shared/config/types/Theme';

import setThemeHelper from '../helpers/setTheme';

const initialState: IThemeSlice = {
	theme: appStorage.getItem(appStorageList.theme) as themeNames || themeNames.LIGHT
}

export const themeSlice = createSlice({
	name: 'theme',
	initialState,
	reducers: {

		setTheme (state, action: PayloadAction<themeNames>) {
			switch (action.payload) {
				case themeNames.LIGHT:
					state.theme = themeNames.LIGHT;
					break;
				case themeNames.DARK:
					state.theme = themeNames.DARK;
					break;
				default:
					state.theme = themeNames.LIGHT;
					break;
			}
			setThemeHelper(state.theme)
			appStorage.setItem(appStorageList.theme, String(state.theme))
			// localStorage.setItem(LOCAL_STORAGE_THEME_KEY, state.theme);
		},

		toggleTheme (state) {
			if (state.theme === themeNames.LIGHT) {
				state.theme = themeNames.DARK
				document.body.classList.add('app_dark_theme');
			} else {
				state.theme = themeNames.LIGHT
				document.body.classList.remove('app_dark_theme');
			}
			setThemeHelper(state.theme)
			appStorage.setItem(appStorageList.theme, String(state.theme))
			// localStorage.setItem(LOCAL_STORAGE_THEME_KEY, state.theme);
		}

	}

});

export const themeSliceReducer = themeSlice.reducer;
export const themeSliceActions = themeSlice.actions;
