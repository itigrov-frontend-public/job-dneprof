import { type DeepPartial } from '@reduxjs/toolkit';
import { type IThemeSlice, themeNames } from 'shared/config/types/Theme';

import { themeSliceActions, themeSliceReducer } from './themeSlice';

describe('themeSlice.test', () => {
	test('setTheme', () => {
		const state: DeepPartial<IThemeSlice> = { theme: themeNames.LIGHT };
		expect(themeSliceReducer(
			state as IThemeSlice,
			themeSliceActions.setTheme(themeNames.DARK)
		)).toEqual({ theme: themeNames.DARK });
	});

	test('toggleTheme', () => {
		const state: DeepPartial<IThemeSlice> = { theme: themeNames.LIGHT };
		expect(themeSliceReducer(
			state as IThemeSlice,
			themeSliceActions.toggleTheme()
		)).toEqual({ theme: themeNames.DARK });
	});
});
