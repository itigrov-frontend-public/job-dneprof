import { themeNames } from 'shared/config/types/Theme';

import setTheme from './setTheme';

describe('classNames', () => {
	//
	test('setTheme: DARK', () => {
		expect(setTheme(themeNames.DARK)).toBe(true);
		expect(document.body.classList.contains('app_dark_theme')).toBe(true);
	});

	test('setTheme: Light', () => {
		expect(setTheme(themeNames.LIGHT)).toBe(true);
		expect(document.body.classList.contains('app_dark_theme')).toBe(false);
	});

	test('setTheme: Bad Theme Param', () => {
		expect(setTheme(themeNames.DARK)).toBe(true);
		expect(document.body.classList.contains('app_dark_theme')).toBe(true);
		// eslint-disable-next-line @typescript-eslint/ban-ts-comment
		// @ts-expect-error
		expect(setTheme('noThemeParam')).toBe(false);
		expect(document.body.classList.contains('app_dark_theme')).toBe(false);
	});
});
