import { themeNames } from 'shared/config/types/Theme';

export default function setTheme (theme: themeNames) {
	let ret = false
	switch (theme) {
		case themeNames.LIGHT:
			document.body.classList.remove('app_dark_theme');
			ret = true;
			break;
		case themeNames.DARK:
			document.body.classList.add('app_dark_theme');
			ret = true;
			break;
		default:
			document.body.classList.remove('app_dark_theme');
			break;
	}
	return ret;
}
