import { type FC, memo, type PropsWithChildren, useState } from 'react'
import { breakpoints } from 'shared/config/types/breakpoints';
import { Navigation, Pagination, Scrollbar, Thumbs } from 'swiper';
import { Swiper, type SwiperClass } from 'swiper/react';

import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';
import './styles/swiper.sass'

import 'swiper/css';

interface SwiperProviderGallaryProps extends PropsWithChildren {
	slides?: JSX.Element[]
	thumbs?: JSX.Element[]
}

const SliderSwiperGallary: FC<SwiperProviderGallaryProps> = ({ slides, thumbs, children }) => {
	const [thumbsSwiper, setThumbsSwiper] = useState<SwiperClass | null>(null);

	return (
		<>
			<Swiper
				className='swiper swiperGallary swiperDesign'
				modules={[Navigation, Pagination, Scrollbar, Thumbs]}
				thumbs={{ swiper: thumbsSwiper && !thumbsSwiper.destroyed ? thumbsSwiper : null }}

				autoHeight
				loop
				grabCursor
				pagination={{ clickable: true }}
				scrollbar={{ draggable: true }}
				// navigation
			>
				{slides}
				{children}
			</Swiper>

			{thumbs && thumbs.length > 1 && <Swiper
				className='swiper swiperGallaryThumbs swiperDesign swiperDesignThumbs'
				modules={[Navigation, Pagination, Scrollbar]}
				onSwiper={setThumbsSwiper}
				spaceBetween={10}

				autoHeight
				loop
				grabCursor
				watchSlidesProgress
				// pagination={{ clickable: true }}
				// scrollbar={{ draggable: true }}
				// navigation
				// navigation={{ nextEl: ' .swiper-button-next', prevEl: ' .swiper-button-prev' }}

				breakpoints={{
					0: {
						slidesPerView: 3,
						slidesPerGroup: 2
					// spaceBetween: 20,
					},
					[breakpoints.tablet]: {
						slidesPerView: 3,
						slidesPerGroup: 2
					// spaceBetween: 20,
					},
					[breakpoints.pc]: {
						slidesPerView: 3,
						slidesPerGroup: 2
					// spaceBetween: 20,
					}
				}}

			>
				{thumbs}
			</Swiper>}
		</>
	)
}

export default memo(SliderSwiperGallary)

// ! Use this code on slider page
// <SliderSwiperGallary slides={gallarySlides} thumbs={gallaryThumbs} />
/*
const gallarySlides = pageCard.images.map((img) => {
	return (
		<SwiperSlide key={`swiperSlides-${img.id}`}>
			<img
			// loading="lazy"
				src={getImageUrl('/catalogCards-list/', img.image, appParam.isWebp, appParam.isRetina)}
				alt={img.name || pageCard.name}
				title={img.name || pageCard.name}
				width={490}
				height={368}
			/>
		</SwiperSlide>
	)
});

const gallaryThumbs = pageCard.images.map((img) => {
	return (
		<SwiperSlide key={`swiperSlides-${img.id}`}>
			<img
			// loading="lazy"
				src={getImageUrl('/catalogCards-list/', img.image, appParam.isWebp, appParam.isRetina)}
				alt={img.name || pageCard.name}
				title={img.name || pageCard.name}
				width={490}
				height={368}
			/>
		</SwiperSlide>
	)
});
*/
