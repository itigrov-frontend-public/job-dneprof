import getLangTyped from './helpers/getLangTyped';
import ChangeLangButton from './ui/ChangeLangButton';

export {
	ChangeLangButton,
	getLangTyped
}
