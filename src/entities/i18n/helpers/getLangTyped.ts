import { useTranslation } from 'react-i18next';
import { i18nNames } from 'shared/config/types/i18n';

const getLangTyped = (lang: string | undefined = undefined) => {
	let langTyped = i18nNames.ru
	switch (lang || useTranslation().i18n.language) {
		case 'ru':
			langTyped = i18nNames.ru
			break;
		case 'en':
			langTyped = i18nNames.en
			break;
		default:
			langTyped = i18nNames.ru
			break;
	}
	return langTyped
}

export default getLangTyped
