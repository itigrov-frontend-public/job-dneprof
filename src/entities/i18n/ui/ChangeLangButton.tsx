import { type FC, type HTMLAttributes, memo, type PropsWithChildren, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { i18nNames } from 'shared/config/types/i18n';
import SvgLangEn from 'shared/static/svg/icons/lang_en.svg'
import SvgLangRu from 'shared/static/svg/icons/lang_ru.svg'
import { Icon } from 'shared/ui/Icon';

// React.MouseEvent<HTMLElement>
interface ChangeLangButtonProps extends PropsWithChildren, HTMLAttributes<HTMLElement> {}

const ChangeLangButton: FC<ChangeLangButtonProps> = (props) => {
	const {
		title,
		...otherProps
	} = props

	const { t, i18n } = useTranslation();

	const toggleLang = useCallback(async () => {
		await i18n.changeLanguage(i18n.language === i18nNames.ru ? i18nNames.en : i18nNames.ru)
	}, []);

	return (
		<Icon
			hover
			onClick={ toggleLang }
			title={title || t('button-ChangeLangButton')}
			{...otherProps}
		>
			{i18n.language === i18nNames.ru ? <SvgLangRu/> : <SvgLangEn/>}
		</Icon>
	);
};
export default memo(ChangeLangButton)
