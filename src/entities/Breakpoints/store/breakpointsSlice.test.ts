import { type DeepPartial } from '@reduxjs/toolkit';
import { type IBreakpointsSlice } from 'shared/config/types/breakpoints';

import { breakpointsSliceActions, breakpointsSliceReducer } from './breakpointsSlice';

describe('breakpointsSlice.test', () => {
	test('breakpointsSlice setMobile', () => {
		const state: DeepPartial<IBreakpointsSlice> = { isMobile: false, isTabletLess: false, isTablet: false, isTabletMore: false, isPc: false };
		expect(breakpointsSliceReducer(
			state as IBreakpointsSlice,
			breakpointsSliceActions.setMobile()
		)).toEqual({ isMobile: true, isTabletLess: true, isTablet: false, isTabletMore: false, isPc: false });
	});

	test('breakpointsSlice setTablet', () => {
		const state: DeepPartial<IBreakpointsSlice> = { isMobile: false, isTabletLess: false, isTablet: false, isTabletMore: false, isPc: false };
		expect(breakpointsSliceReducer(
			state as IBreakpointsSlice,
			breakpointsSliceActions.setTablet()
		)).toEqual({ isMobile: false, isTabletLess: true, isTablet: true, isTabletMore: true, isPc: false });
	});

	test('breakpointsSlice setPc', () => {
		const state: DeepPartial<IBreakpointsSlice> = { isMobile: false, isTabletLess: false, isTablet: false, isTabletMore: false, isPc: false };
		expect(breakpointsSliceReducer(
			state as IBreakpointsSlice,
			breakpointsSliceActions.setPc()
		)).toEqual({ isMobile: false, isTabletLess: false, isTablet: false, isTabletMore: true, isPc: true });
	});
});
