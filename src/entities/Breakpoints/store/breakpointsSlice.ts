import { createSlice } from '@reduxjs/toolkit';
import { breakpoints, type IBreakpointsSlice } from 'shared/config/types/breakpoints';

const width = window.innerWidth
const initialState: IBreakpointsSlice = {
	isMobile: width < breakpoints.tablet,
	isTabletLess: width < breakpoints.pc, // tablet + mobile
	isTablet: width >= breakpoints.tablet && width < breakpoints.pc,
	isTabletMore: width >= breakpoints.tablet, // tablet + pc
	isPc: width >= breakpoints.pc
}

export const breakpointsSlice = createSlice({
	name: 'breakpoints',
	initialState,
	reducers: {

		setMobile (state) {
			state.isMobile = true;
			state.isTabletLess = true;
			state.isTablet = false;
			state.isTabletMore = false;
			state.isPc = false;

			// __IS_DEV__ && console.log('breakpointsSlice setMobile', state);
		},
		setTablet (state) {
			state.isMobile = false;
			state.isTabletLess = true;
			state.isTablet = true;
			state.isTabletMore = true;
			state.isPc = false;

			// __IS_DEV__ && console.log('breakpointsSlice setTablet', state);
		},
		setPc (state) {
			state.isMobile = false;
			state.isTabletLess = false;
			state.isTablet = false;
			state.isTabletMore = true;
			state.isPc = true;

			// __IS_DEV__ && console.log('breakpointsSlice setPc', state);
		}

	}

});

export const breakpointsSliceReducer = breakpointsSlice.reducer; // for import in rootReducer
export const breakpointsSliceActions = breakpointsSlice.actions; // for actions in "useAction"
