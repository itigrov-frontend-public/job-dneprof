import { type StateSchema } from 'shared/config/store/AppStore';

export const getPoints = (state: StateSchema) => state.breakpoints;
