export default function setHeaderPadding () {
	const headerIn: HTMLElement | null = document.querySelector('[data-headerin]') || null;
	const header: HTMLElement | null = document.querySelector('[data-header]') || null;

	if (header && headerIn) {
		header.style.paddingBottom = String(headerIn.offsetHeight) + 'px';
	}
}
