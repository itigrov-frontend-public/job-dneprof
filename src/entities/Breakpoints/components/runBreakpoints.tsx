import { useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { breakpoints } from 'shared/config/types/breakpoints';

import setHeaderPadding from '../helpers/setHeaderPadding';
import { breakpointsSliceActions } from '../store/breakpointsSlice';

const runBreakpoints = () => {
	const dispatch = useDispatch()
	// const point = useSelector(getPoints)

	const setMobile = useCallback((e: MediaQueryListEvent) => {
		if (e.matches) {
			dispatch(breakpointsSliceActions.setMobile());
			setHeaderPadding()
		}
	}, [dispatch]);

	const setTablet = useCallback((e: MediaQueryListEvent) => {
		if (e.matches) {
			dispatch(breakpointsSliceActions.setTablet());
			setHeaderPadding()
		}
	}, [dispatch]);

	const setPc = useCallback((e: MediaQueryListEvent) => {
		if (e.matches) {
			dispatch(breakpointsSliceActions.setPc());
			setHeaderPadding()
		}
	}, [dispatch]);

	useEffect(() => {
		// __IS_DEV__ && console.log('entities/runBreakpoints useEffect', point);

		const mobile = window.matchMedia('(width < ' + String(breakpoints.tablet) + 'px)');
		const tablet = window.matchMedia('( (width >= ' + String(breakpoints.tablet) + 'px) and (width < ' + String(breakpoints.pc) + 'px) )');
		const pc = window.matchMedia('(width >= ' + String(breakpoints.pc) + 'px)');

		mobile.addEventListener('change', setMobile);
		tablet.addEventListener('change', setTablet);
		pc.addEventListener('change', setPc);

		return () => {
			mobile.removeEventListener('change', setMobile);
			tablet.removeEventListener('change', setTablet);
			pc.removeEventListener('change', setPc);
		};
	}, []);
};

export default runBreakpoints
