import { type FC, type InputHTMLAttributes, memo, useCallback, useEffect, useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom';

import InputSearch from './InputSearch';

interface InputSearchUrlProps extends Omit<InputHTMLAttributes<HTMLInputElement>, 'value' | 'onChange'> {
	value?: string,
	defaultValue?: string,
	onChange: (value: string) => void;
}

const InputSearchUrl: FC<InputSearchUrlProps> = ({ value = '', defaultValue = '', onChange, ...otherProps }) => {
	const [valueUseState, setValue] = useState<string>(defaultValue); // input value
	const location = useLocation();
	const navigate = useNavigate();

	// * Alternative variant in comment
	// const [urlParams] = useSearchParams();
	// const searchUrl = urlParams.get('search') || defaultValue
	const searchUrl = new URLSearchParams(location.search).get('search') || defaultValue;

	useEffect(() => {
		if (searchUrl !== value) {
			onChange(searchUrl); // DB
			setValue(searchUrl); // input val
		}
	}, [location.pathname, location.search])
	// }, [window.location.pathname])

	const onChangeInput = useCallback((value: string) => {
		onChange(value)
		navigate({
			pathname: window.location.pathname,
			search: '?search=' + value
		});
	}, [])

	return (
		<InputSearch
			valueUseState={valueUseState}
			setValue={setValue}
			onChange={onChangeInput}
			value={value}
			// defaultValue={search}
			{...otherProps}
		/>
	)
}
export default memo(InputSearchUrl)
