import React, { type FC, type InputHTMLAttributes, memo, useEffect } from 'react'
import { searchOnChangeTimeout } from 'shared/config/const';

import '../../shared/config/styles/components/form.scss'
import cn from './inputSearch.module.scss'

interface InputSearchProps extends Omit<InputHTMLAttributes<HTMLInputElement>, 'value' | 'onChange'> {
	value?: string,
	defaultValue?: string,
	onChange: (value: string) => void;
	setValue: React.Dispatch<React.SetStateAction<string>>
	valueUseState: string
}

const InputSearch: FC<InputSearchProps> = ({ value = '', defaultValue = '', onChange, valueUseState, setValue, ...otherProps }) => {
	useEffect(() => {
		if (valueUseState !== value) {
			const myTimeout = setTimeout(() => {
				onChange(valueUseState)
			}, searchOnChangeTimeout);

			return () => {
				clearTimeout(myTimeout);
			};
		}
	}, [valueUseState])

	return (
		<div className={cn.catalogSearch}>
			<input
				type="text"
				value={valueUseState}
				onChange={(event) => { setValue(event.target.value) }}
				{...otherProps}
			/>
		</div>
	)
}

export default memo(InputSearch)
