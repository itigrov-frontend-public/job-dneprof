import InputSearch from './InputSearch';
import InputSearchUrl from './InputSearchUrl';

export {
	InputSearch,
	InputSearchUrl
}
