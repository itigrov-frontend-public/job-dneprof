import { type StateSchema } from 'shared/config/store/AppStore';

export const getFavouriteIds = (state: StateSchema) => state.favourite?.ids;
