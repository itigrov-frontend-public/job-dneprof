import { type StateSchema } from 'shared/config/store/AppStore';

export const getFavouriteCount = (state: StateSchema) => Number(state.favourite?.count);
