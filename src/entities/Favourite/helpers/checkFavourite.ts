const checkFavourite = (ids: number[], id: number) => {
	return ids.includes(id);
};

export default checkFavourite
