import { type FC, type HTMLAttributes, memo } from 'react';
import { useSelector } from 'react-redux';
import SvgHeart from 'shared/static/svg/icons/heart.svg'
import { IconLink } from 'shared/ui/Icon';

import { getFavouriteCount } from '../selectors/getFavouriteCount';

const FavouriteIcon: FC<HTMLAttributes<HTMLElement>> = (props) => {
	const {
		...otherProps
	} = props
	const favouriteCount = useSelector(getFavouriteCount);

	return (
		<IconLink to={'/favourite'} alert={favouriteCount} {...otherProps}><SvgHeart/></IconLink>
	)
}

export default memo(FavouriteIcon)
