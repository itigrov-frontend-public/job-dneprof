import { createSlice, type PayloadAction } from '@reduxjs/toolkit';
import { appStorage, appStorageList } from 'shared/components/appStorage';
import { type IFavouriteSlice } from 'shared/config/types/Favourite';

const storageFavourite = JSON.parse(appStorage.getItem(appStorageList.favourite) || '[]')

const initialState: IFavouriteSlice = {
	count: storageFavourite.length,
	ids: storageFavourite
}

export const favouriteSlice = createSlice({
	name: 'favourite',
	initialState,
	reducers: {
		/*
		addFavourite (state, action: PayloadAction<number>) {
			state.ids.push(action.payload)
			state.count = state.ids.length
			appStorage.setItem(appStorageList.favourite, JSON.stringify(state.ids))
		},

		removeFavourite (state, action: PayloadAction<number>) {
			const index = state.ids.indexOf(action.payload)
			if (index > -1) { state.ids.splice(index, 1) }

			state.count = state.ids.length
			appStorage.setItem(appStorageList.favourite, JSON.stringify(state.ids))
		},
		*/
		toggleFavourite (state, action: PayloadAction<number>) {
			if (state.ids.includes(action.payload)) {
				const index = state.ids.indexOf(action.payload)
				if (index > -1) { state.ids.splice(index, 1) }

				state.count = state.ids.length
				appStorage.setItem(appStorageList.favourite, JSON.stringify(state.ids))
			} else {
				state.ids.push(action.payload)
				state.count = state.ids.length
				appStorage.setItem(appStorageList.favourite, JSON.stringify(state.ids))
			}
		}

	}

});

export const favouriteSliceReducer = favouriteSlice.reducer;
export const favouriteSliceActions = favouriteSlice.actions;
