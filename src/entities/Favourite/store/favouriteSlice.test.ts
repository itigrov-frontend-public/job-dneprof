import { type DeepPartial } from '@reduxjs/toolkit';
import { type IFavouriteSlice } from 'shared/config/types/Favourite';

import { favouriteSliceActions, favouriteSliceReducer } from './favouriteSlice';

describe('favouriteSlice.test', () => {
	test('favouriteSlice add 777', () => {
		const state: DeepPartial<IFavouriteSlice> = { count: 0, ids: [] };
		expect(favouriteSliceReducer(
			state as IFavouriteSlice,
			favouriteSliceActions.toggleFavourite(777)
		)).toEqual({ count: 1, ids: [777] });
	});

	test('favouriteSlice add 5', () => {
		const state: DeepPartial<IFavouriteSlice> = { count: 1, ids: [777] };
		expect(favouriteSliceReducer(
			state as IFavouriteSlice,
			favouriteSliceActions.toggleFavourite(5)
		)).toEqual({ count: 2, ids: [777, 5] });
	});

	test('favouriteSlice remove 5', () => {
		const state: DeepPartial<IFavouriteSlice> = { count: 2, ids: [777, 5] };
		expect(favouriteSliceReducer(
			state as IFavouriteSlice,
			favouriteSliceActions.toggleFavourite(5)
		)).toEqual({ count: 1, ids: [777] });
	});

	test('favouriteSlice remove 777', () => {
		const state: DeepPartial<IFavouriteSlice> = { count: 1, ids: [777] };
		expect(favouriteSliceReducer(
			state as IFavouriteSlice,
			favouriteSliceActions.toggleFavourite(777)
		)).toEqual({ count: 0, ids: [] });
	});
});
