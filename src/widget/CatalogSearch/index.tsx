import { type FC, memo, Suspense, useState } from 'react'
import { useTranslation } from 'react-i18next';
import { InputSearchUrl } from 'entities/InputSearch';
import { CatalogCardsLazy } from 'features/Catalog/CatalogCardsLazy';
import Loader from 'shared/ui/Loader';

const CatalogSearch: FC = () => {
	const [searchValue, setSearchValue] = useState<string>('');
	const { t } = useTranslation()

	return (
		<>
			<InputSearchUrl
				onChange={setSearchValue}
				value={searchValue}
				// defaultValue={search}
				placeholder={t('Search-placeholder')}
			/>
			{searchValue && <p className="pageName">{t('Search-cards')}: {searchValue}</p>}
			{searchValue.length > 0 &&
				<Suspense fallback={<Loader/>}>
					<div className="pBotBox">
						<CatalogCardsLazy
							search={searchValue.toString()}
							noCardsText={t('Search-noCards')}
						/>
					</div>
				</Suspense>
			}
		</>
	)
}

export default memo(CatalogSearch)
