import { type FC, lazy, memo, Suspense } from 'react';
import Loader from 'shared/ui/Loader';

import cn from '../styles/footer.module.scss';

const FooterUi = lazy<FC>(async () => await import('./FooterUi'));

const Footer: FC = () => {
	return (
		<footer className={cn.footer}>
			<Suspense fallback={<Loader />}>
				<FooterUi />
			</Suspense>
		</footer>
	)
}

export default memo(Footer)
