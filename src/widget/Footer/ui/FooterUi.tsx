import { type FC, memo } from 'react';
import SvgPhone from 'shared/static/svg/icons/phone.svg'
import { IconTextLink } from 'shared/ui/Icon';
import Logo from 'shared/ui/Logo';

import cn from '../styles/footer.module.scss';

const FooterUi: FC = () => {
	return (
		<div className="mw">
			<div className={cn.footer_in}>

				<div className={cn.footer_left}>
					<Logo />
				</div>
				{/* <div className={cn.footer_center}></div> */}
				<div className={cn.footer_right}>
					<IconTextLink text='8 (901) 993-51-93' title="8 (901) 993-51-93" to='tel:+79019935193' icon={<SvgPhone/>} />
				</div>

			</div>
		</div>
	)
}

export default memo(FooterUi)
