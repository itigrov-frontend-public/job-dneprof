
import type { Meta, StoryObj } from '@storybook/react';
import { i18nNames } from 'shared/config/types/i18n';
import { themeNames } from 'shared/config/types/Theme';
import { I18nDecorator, ThemeDecoratorStory } from 'shared/devTests/storybook';

import Footer from './ui/Footer';

const meta = {
	title: 'Widget/Footer',
	component: Footer,
	argTypes: {
	},
	args: {},
	parameters: {
		layout: undefined // centered | fullscreen | undefined
	}

} satisfies Meta<typeof Footer>;

export default meta;

type Story = StoryObj<typeof meta>;

export const FooterAll: Story = {};
FooterAll.decorators = [
	(Story, context) => ThemeDecoratorStory(Story, context),
	(Story, context) => I18nDecorator(Story, context)
];

export const FooterEnDark: Story = {};
FooterEnDark.decorators = [
	(Story, context) => ThemeDecoratorStory(Story, context, themeNames.DARK),
	(Story, context) => I18nDecorator(Story, context, i18nNames.en)
];
