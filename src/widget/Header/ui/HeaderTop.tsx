import { type FC, memo, useEffect } from 'react';
import { setHeaderPadding } from 'entities/Breakpoints';
import SvgPhone from 'shared/static/svg/icons/phone.svg'
import { IconTextLink } from 'shared/ui/Icon';
import Logo from 'shared/ui/Logo';

import HeaderTopPc from './breakpoints/HeaderTopPc';

import cn from '../styles/header.module.scss';

const HeaderTop: FC = () => {
	useEffect(() => {
		setHeaderPadding()
	}, [])

	return (
		<div className={cn.headerTop}>
			<div className="mw">
				<div className={cn.headerTop_in}>

					<div className={cn.headerTop_left}>
						<Logo />
					</div>
					{ /* <div className={cn.headerTop_center}></div> */ }
					<div className={cn.headerTop_right}>
						<HeaderTopPc />
						<IconTextLink
							text='8 (901) 993-51-93'
							title='8 (901) 993-51-93'
							to='tel:+79019935193' icon={<SvgPhone/>}
						/>
					</div>

				</div>
			</div>
		</div>
	)
}

export default memo(HeaderTop)
