import { type FC, lazy, memo, Suspense } from 'react';
import Loader from 'shared/ui/Loader';

import cn from '../styles/header.module.scss';

const HeaderTop = lazy(async () => await import('./HeaderTop'))
const HeaderBottom = lazy(async () => await import('./HeaderBottom'))

const Header: FC = () => {
	return (
		<header data-header className={cn.header}>
			<div data-headerin className={cn.header_in}>
				<Suspense fallback={<Loader />}>
					<HeaderTop />
				</Suspense>
				<Suspense fallback={<Loader />}>
					<HeaderBottom />
				</Suspense>
			</div>
		</header>
	)
}

export default memo(Header)
