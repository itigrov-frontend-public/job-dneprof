import { type FC, memo } from 'react'
import { useSelector } from 'react-redux'
import { getPoints } from 'entities/Breakpoints'
import { MenuOpenButton } from 'features/Menu'

const HeaderBotMobile: FC = () => {
	const points = useSelector(getPoints)

	return points.isMobile && <MenuOpenButton />
}

export default memo(HeaderBotMobile)
