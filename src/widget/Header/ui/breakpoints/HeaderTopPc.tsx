import { type FC, memo } from 'react'
import { useSelector } from 'react-redux';
import { getPoints } from 'entities/Breakpoints';
import { ChangeLangButton } from 'entities/i18n';
import { ChangeThemeButton } from 'entities/Theme';

const HeaderTopPc: FC = () => {
	const points = useSelector(getPoints)

	return points.isTabletMore && <>
		<ChangeThemeButton/>
		<ChangeLangButton/>
	</>
}

export default memo(HeaderTopPc)
