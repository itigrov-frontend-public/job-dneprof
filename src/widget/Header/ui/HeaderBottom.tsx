import { type FC, memo, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { getPoints, setHeaderPadding } from 'entities/Breakpoints';
import { FavouriteIcon } from 'entities/Favourite';
import { Menu } from 'features/Menu';
import SvgAvito from 'shared/static/svg/avito.svg'
import SvgCatalog from 'shared/static/svg/icons/category.svg'
import { ButtonIcon } from 'shared/ui/Button';
import { IconLink } from 'shared/ui/Icon';

import HeaderBotMobile from './breakpoints/HeaderBotMobile';

import cn from '../styles/header.module.scss';

const HeaderBottom: FC = () => {
	const { t } = useTranslation()
	const points = useSelector(getPoints)
	useEffect(() => {
		setHeaderPadding()
	}, [])

	// __IS_DEV__ && console.log('HeaderBottom');

	return (

		<div className={cn.headerBot}>
			<div className="mw">
				<div className={cn.headerBot_in}>

					<div className={cn.headerBot_left}>
						<Link to='/catalog' title={t('buttonCatalog')}>
							{points.isMobile
								? <ButtonIcon title={t('buttonCatalog')} icon={<SvgCatalog/>}></ButtonIcon>
								: <ButtonIcon title={t('buttonCatalog')} icon={<SvgCatalog/>}>{t('buttonCatalog')}</ButtonIcon>
							}
						</Link>
					</div>
					<div className={cn.headerBot_center}>
						<Menu />
					</div>
					<div className={cn.headerBot_right}>
						<IconLink
							to='https://www.avito.ru/user/da6364cea0b52f4d0d1c7ad3f18d120e/profile'
							target='_blank'
							title='Avito'
						><SvgAvito/></IconLink>

						<FavouriteIcon title={t('button-FavouriteIcon')} />
						<HeaderBotMobile/>
					</div>

				</div>
			</div>
		</div>
	)
}

export default memo(HeaderBottom)
