
import type { Meta, StoryObj } from '@storybook/react';
import { i18nNames } from 'shared/config/types/i18n';
import { themeNames } from 'shared/config/types/Theme';
import { I18nDecorator, ThemeDecoratorStory } from 'shared/devTests/storybook';

import Header from './ui/Header';

const meta = {
	title: 'Widget/Header',
	component: Header,
	argTypes: {
	},
	args: {
		// code: ContentErrorCode.error0
	},
	parameters: {
		layout: 'fullscreen' // centered | fullscreen | undefined
	}

} satisfies Meta<typeof Header>;

export default meta;

type Story = StoryObj<typeof meta>;

export const HeaderAll: Story = { };
HeaderAll.decorators = [
	(Story, context) => ThemeDecoratorStory(Story, context),
	(Story, context) => I18nDecorator(Story, context)
];

export const HeaderEnDark: Story = {};
HeaderEnDark.decorators = [
	(Story, context) => ThemeDecoratorStory(Story, context, themeNames.DARK),
	(Story, context) => I18nDecorator(Story, context, i18nNames.en)
];

// const HeaderStore = (args) => {
// useAddStore('menu', menuSliceReducer)
// const dispatch = useDispatch();
// useEffect(() => {
// dispatch(breakpointsSliceActions.setMobile());
// dispatch(menuSliceActions.setOpen(true))
// })

// return (
// <Header {...args} />
// )
// }

// export const HeaderAll: Story = HeaderStore //StorybookAddMenuSlice();
