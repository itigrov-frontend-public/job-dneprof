import { type FC, memo } from 'react'
import { useParams } from 'react-router-dom'
import getCatalogCard from 'features/Catalog/api/getCatalogCard'
import CatalogCardPageUi from 'features/Catalog/ui/CatalogCardPageUi'
import ErrorPage from 'pages/ErrorPage'
import Seo from 'shared/components/Seo'

const CatalogCard: FC = () => {
	const params = useParams()
	const huu = String(params.huu)
	const groupHuu = String(params.groupHuu)

	const { data: pageCard, isLoading, isError } = getCatalogCard(huu, groupHuu)
	if (!pageCard && !isLoading && !isError) { return (<ErrorPage />) }

	// __IS_DEV__ && console.log('pages/CatalogCard ', huu, groupHuu);

	return (
		<>
			{pageCard && <Seo title={pageCard.name} />}
			<CatalogCardPageUi pageCard={pageCard} isError={isError} isLoading={isLoading} />
		</>
	)
}

export default memo(CatalogCard)
