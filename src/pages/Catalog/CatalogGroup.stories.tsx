
import type { Meta, StoryObj } from '@storybook/react';
import { i18nNames } from 'shared/config/types/i18n';
import { themeNames } from 'shared/config/types/Theme';
import { I18nDecorator, ThemeDecoratorStory } from 'shared/devTests/storybook';

import Catalog from '.';

const meta = {
	title: 'pages/Catalog/group',
	component: Catalog,
	argTypes: {},
	args: {},
	// decorators: [withRouter],
	// decorators: [ThemeDecoratorStory],
	parameters: {
		reactRouter: {
			// routePath: '/delivery'
			routePath: '/catalog/:huu',
			routeParams: { huu: 'accessories' }
		},

		backgrounds: {
			// default: 'dark'
		},
		layout: 'fullscreen' // centered | fullscreen | undefined
	}

} satisfies Meta<typeof Catalog>;

export default meta;

type Story = StoryObj<typeof meta>;

export const CatalogAll: Story = { args: {} };
CatalogAll.decorators = [
	(Story, context) => ThemeDecoratorStory(Story, context),
	(Story, context) => I18nDecorator(Story, context)
];

export const CatalogEnDark: Story = { args: {} };
CatalogEnDark.decorators = [
	(Story, context) => ThemeDecoratorStory(Story, context, themeNames.DARK),
	(Story, context) => I18nDecorator(Story, context, i18nNames.en)
];
