import { type FC, memo, Suspense, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
import { InputSearchUrl } from 'entities/InputSearch'
import { getCatalogGroup } from 'features/Catalog'
import { CatalogCardsLazy } from 'features/Catalog/CatalogCardsLazy'
import { CatalogGroupsLazy } from 'features/Catalog/CatalogGroupsLazy'
import ErrorPage from 'pages/ErrorPage'
import Seo from 'shared/components/Seo'
import { classNames } from 'shared/helpers/classNames'
import ErrorLoading from 'shared/ui/ErrorLoading'
import Loader from 'shared/ui/Loader'

const Catalog: FC = () => {
	const params = useParams()
	if (String(params.huu).toLowerCase().startsWith('-system-')) { return (<ErrorPage />) }
	const catalogMainPage = params.huu === undefined
	const huu = params.huu || '-system-root'

	const { data: pageGroup, isLoading, isError } = getCatalogGroup(huu)
	if (!pageGroup && !isLoading && !isError) { return (<ErrorPage />) }
	const { t } = useTranslation()
	const [searchValue, setSearchValue] = useState<string>('');

	// __IS_DEV__ && console.log('pages/Catalog', huu);

	return (
		<>
			{pageGroup && <Seo title={pageGroup.name2 || pageGroup.name} />}
			<section className={classNames('maxWidth mainSection', {}, [])} >
				<div className="mainSection_in pBotBox">

					{isLoading && <Loader />}
					{isError && <ErrorLoading/>}
					{pageGroup && <p className="pageName">{pageGroup.name2 || pageGroup.name}</p>}
					<Suspense fallback={<Loader/>}><CatalogGroupsLazy parentHuu={huu} currentHuu={huu}/></Suspense>

					<InputSearchUrl
						onChange={setSearchValue}
						value={searchValue}
						placeholder={t('Search-placeholder')}
					/>

					{(!catalogMainPage || searchValue !== '') && <Suspense fallback={<Loader/>}>
						<CatalogCardsLazy search={searchValue.toString()} groupHuu={!catalogMainPage ? huu : undefined} noCardsText={t('Catalog-noCards')}/>
					</Suspense>}

				</div>
			</section>
		</>
	)
}

export default memo(Catalog)
