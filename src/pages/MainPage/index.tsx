import { type FC, memo, Suspense } from 'react'
import { useTranslation } from 'react-i18next'
import Seo from 'shared/components/Seo'
import { classNames } from 'shared/helpers/classNames'
import Loader from 'shared/ui/Loader'
import CatalogSearch from 'widget/CatalogSearch'

import MainCatalogCards from './conmonents/MainCatalogCards'

const MainPage: FC = () => {
	const { t } = useTranslation()

	// __IS_DEV__ && console.log('pages/MainPage');

	return (
		<>
			<Seo title={t('title-MainPage')} />
			<section className={classNames('maxWidth mainSection', {}, [])} >
				<div className="mainSection_in">

					<CatalogSearch />

					<Suspense fallback={<Loader/>}>
						<MainCatalogCards/>
					</Suspense>

				</div>
			</section>
		</>
	)
}

export default memo(MainPage)
