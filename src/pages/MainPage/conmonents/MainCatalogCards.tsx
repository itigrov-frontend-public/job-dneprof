import { type FC, memo } from 'react'
import { useTranslation } from 'react-i18next';
import { getCatalogCards } from 'features/Catalog';
import { CatalogCardsUiLazy } from 'features/Catalog/CatalogCardsUiLazy';
import ErrorLoading from 'shared/ui/ErrorLoading';
import Loader from 'shared/ui/Loader';

const MainCatalogCards: FC = () => {
	const { data: cards, isLoading, isError } = getCatalogCards({ onIndex: 1, limit: 30 })
	const { t } = useTranslation()

	// __IS_DEV__ && console.log('MainCatalogCards');

	return (
		<>
			{ isLoading && <Loader /> }
			{ isError && <ErrorLoading/> }
			{ cards && cards.length > 0 && <>
				<p className="pageName">{t('MainPage-popularCards')}:</p>
				<CatalogCardsUiLazy cards={cards} />
			</> }
		</>
	)
}

export default memo(MainCatalogCards)
