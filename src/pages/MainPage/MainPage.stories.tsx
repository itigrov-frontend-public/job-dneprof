
import type { Meta, StoryObj } from '@storybook/react';
import { i18nNames } from 'shared/config/types/i18n';
// import { i18nNames } from 'shared/config/types/i18n';
import { themeNames } from 'shared/config/types/Theme';
import { I18nDecorator, ThemeDecoratorStory } from 'shared/devTests/storybook';

import MainPage from '.';

const meta = {
	title: 'pages/Main',
	component: MainPage,
	argTypes: {},
	args: {},
	parameters: {
		backgrounds: {
			// default: 'dark'
		},
		layout: 'fullscreen' // centered | fullscreen | undefined
	}

} satisfies Meta<typeof MainPage>;

export default meta;

type Story = StoryObj<typeof meta>;

export const MainPageAll: Story = { args: {} };
MainPageAll.decorators = [
	(Story, context) => ThemeDecoratorStory(Story, context),
	(Story, context) => I18nDecorator(Story, context)
];

export const MainPageEnDark: Story = { args: {} };
MainPageEnDark.decorators = [
	(Story, context) => ThemeDecoratorStory(Story, context, themeNames.DARK),
	(Story, context) => I18nDecorator(Story, context, i18nNames.en)
];
