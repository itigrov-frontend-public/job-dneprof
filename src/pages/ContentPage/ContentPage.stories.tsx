
import type { Meta, StoryObj } from '@storybook/react';
import { i18nNames } from 'shared/config/types/i18n';
import { themeNames } from 'shared/config/types/Theme';
import { I18nDecorator, ThemeDecoratorStory } from 'shared/devTests/storybook';

import ContentPage from '.';

const meta = {
	title: 'pages/Content',
	component: ContentPage,
	argTypes: {},
	args: {},
	// decorators: [withRouter],
	// decorators: [ThemeDecoratorStory],
	parameters: {
		reactRouter: {
			// routePath: '/delivery'
			routePath: '/:huu',
			routeParams: { huu: 'delivery' }
		},

		backgrounds: {
			// default: 'dark'
		},
		layout: 'fullscreen' // centered | fullscreen | undefined
	}

} satisfies Meta<typeof ContentPage>;

export default meta;

type Story = StoryObj<typeof meta>;

export const ContentPageAll: Story = { args: {} };
ContentPageAll.decorators = [
	(Story, context) => ThemeDecoratorStory(Story, context),
	(Story, context) => I18nDecorator(Story, context)
];

export const ContentPageEnDark: Story = { args: {} };
ContentPageEnDark.decorators = [
	(Story, context) => ThemeDecoratorStory(Story, context, themeNames.DARK),
	(Story, context) => I18nDecorator(Story, context, i18nNames.en)
];
