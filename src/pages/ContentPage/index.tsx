import { type FC, memo } from 'react'
import { useParams } from 'react-router-dom'
import { ContentCardUi, getContentCard } from 'features/Content'
import ErrorPage from 'pages/ErrorPage'
import Seo from 'shared/components/Seo'

const ContentPage: FC = () => {
	const params = useParams()
	if (!params.huu || String(params.huu).toLowerCase().startsWith('-system-')) { return (<ErrorPage />) }

	const { data: pageCard, isLoading, isError } = getContentCard(String(params.huu))
	if (!pageCard && !isLoading && !isError) { return (<ErrorPage />) }

	// __IS_DEV__ && console.log('pages/ContentPage');

	return (
		<>
			{pageCard && <Seo title={pageCard.name} />}
			<ContentCardUi pageCard={pageCard} isLoading={isLoading} isError={isError} />
		</>
	)
}

export default memo(ContentPage)
