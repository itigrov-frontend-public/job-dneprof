import { type FC, memo } from 'react'
import { useTranslation } from 'react-i18next'
import { ContentError } from 'features/ContentError'
import { ContentErrorCode } from 'features/ContentError/type/ContentError'
import Seo from 'shared/components/Seo'

const ErrorPage: FC = () => {
	const { t } = useTranslation('error')

	// __IS_DEV__ && console.log('pages/errorPage');

	return (
		<>
			<Seo title={t('title-404')} />
			<ContentError code={ContentErrorCode.error404}/>
		</>
	)
}

export default memo(ErrorPage)
