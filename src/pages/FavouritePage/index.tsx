import { type FC, memo } from 'react'
import { getContentCard } from 'features/Content'
import ErrorPage from 'pages/ErrorPage'
import Seo from 'shared/components/Seo'
import { classNames } from 'shared/helpers/classNames'
import ErrorLoading from 'shared/ui/ErrorLoading'
import Loader from 'shared/ui/Loader'

import FavouritePageCards from './components/FavouritePageCards'

const FavouritePage: FC = () => {
	const pageHuu = '-system-favourite'
	const { data: pageCard, isLoading, isError } = getContentCard(pageHuu)
	if (!pageCard && !isLoading && !isError) { return (<ErrorPage />) }

	// __IS_DEV__ && console.log('pages/FavouritePage');

	return (
		<>
			{pageCard && <Seo title={pageCard.name} />}
			<section className={classNames('maxWidth mainSection', {}, [])} >
				<div className="mainSection_in pBotBox">

					{isLoading && <Loader />}
					{isError && <ErrorLoading/>}
					{pageCard && <p className="pageName">{pageCard.name}</p>}

					<FavouritePageCards />

				</div>
			</section>
		</>
	)
}

export default memo(FavouritePage)
