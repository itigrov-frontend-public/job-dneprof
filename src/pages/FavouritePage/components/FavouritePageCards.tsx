import { type FC, memo, Suspense } from 'react'
import { useTranslation } from 'react-i18next'
import { useSelector } from 'react-redux'
import { getFavouriteIds } from 'entities/Favourite'
import { CatalogCardsLazy } from 'features/Catalog/CatalogCardsLazy'
import Loader from 'shared/ui/Loader'

const FavouritePageCards: FC = () => {
	const favouriteIds = useSelector(getFavouriteIds)
	const { t } = useTranslation()

	// __IS_DEV__ && console.log('pages/FavouritePageCards');

	return (
		<>
			<Suspense fallback={<Loader/>}>
				<CatalogCardsLazy
					favourite={favouriteIds}
					noCardsText={t('FavouritePage-noCards')}
				/>
			</Suspense>
		</>
	)
}

export default memo(FavouritePageCards)
