
import { MINIMAL_VIEWPORTS } from '@storybook/addon-viewport';
import type { Meta, StoryObj } from '@storybook/react';
import { i18nNames } from 'shared/config/types/i18n';
// import { i18nNames } from 'shared/config/types/i18n';
import { themeNames } from 'shared/config/types/Theme';
import { I18nDecorator, ThemeDecoratorStory } from 'shared/devTests/storybook';

import MenuMobileWrapperUi from './ui/MenuMobileWrapperUi';

const meta = {
	title: 'features/menu/Mobile',
	component: MenuMobileWrapperUi,
	argTypes: {
	},
	args: {
		// code: ContentErrorCode.error0
	},
	parameters: {
		layout: undefined, // centered | fullscreen | undefined
		viewport: {
			defaultViewport: 'mobile1', // 'mobile1', 'mobile2', 'tablet' // import { INITIAL_VIEWPORTS, MINIMAL_VIEWPORTS } from '@storybook/addon-viewport';
			viewports: {
				...MINIMAL_VIEWPORTS // INITIAL_VIEWPORTS, MINIMAL_VIEWPORTS, DEFAULT_VIEWPORT
			}

		}
	}

} satisfies Meta<typeof MenuMobileWrapperUi>;

export default meta;

type Story = StoryObj<typeof meta>;

export const MenuMobileAll: Story = { args: { open: true, titleClose: 'Меню' } };
MenuMobileAll.decorators = [
	(Story, context) => ThemeDecoratorStory(Story, context),
	(Story, context) => I18nDecorator(Story, context)
];

export const MenuMobileEnDark: Story = { args: { open: true, titleClose: 'Меню' } };
MenuMobileEnDark.decorators = [
	(Story, context) => ThemeDecoratorStory(Story, context, themeNames.DARK),
	(Story, context) => I18nDecorator(Story, context, i18nNames.en)
];
