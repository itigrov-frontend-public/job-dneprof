import { type DeepPartial } from '@reduxjs/toolkit';
import { type IMenuSlice } from 'shared/config/types/Menu';

import { menuSliceActions, menuSliceReducer } from './menuSlice';

describe('menuSlice.test', () => {
	test('setMenuOpen true', () => {
		const state: DeepPartial<IMenuSlice> = { open: false };
		expect(menuSliceReducer(
			state as IMenuSlice,
			menuSliceActions.setMenuOpen(true)
		)).toEqual({ open: true });
	});

	test('setMenuOpen false', () => {
		const state: DeepPartial<IMenuSlice> = { open: false };
		expect(menuSliceReducer(
			state as IMenuSlice,
			menuSliceActions.setMenuOpen(false)
		)).toEqual({ open: false });
	});

	test('toggleMenuOpen', () => {
		const state: DeepPartial<IMenuSlice> = { open: false };
		expect(menuSliceReducer(
			state as IMenuSlice,
			menuSliceActions.toggleMenuOpen()
		)).toEqual({ open: true });
	});

	test('toggleMenuOpen', () => {
		const state: DeepPartial<IMenuSlice> = { open: true };
		expect(menuSliceReducer(
			state as IMenuSlice,
			menuSliceActions.toggleMenuOpen()
		)).toEqual({ open: false });
	});
});
