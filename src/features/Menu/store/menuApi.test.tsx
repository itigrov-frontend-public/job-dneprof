import { renderHook } from '@testing-library/react';
import { apiMenuCards } from 'shared/config/store/api';
import { i18nNames } from 'shared/config/types/i18n';
import { fetchMock, RTKQueryApiExpect, StoreDecorator } from 'shared/devTests/unit';

import { menuApi } from './menuApi';

const wrapper = StoreDecorator;

// useGetCardsQuery
describe('menuApi: useGetMenuCardsQuery', () => {
	const data = {}
	const endpointName = 'getMenuCards';
	const dataKeys = ['id', 'parentId', 'name', 'url']

	beforeEach(() => {
		fetchMock.resetMocks();
		fetchMock.mockOnceIf(apiMenuCards, async () =>
			await Promise.resolve({ status: 200, body: JSON.stringify({ data }) })
		);
		fetchMock.mockOnceIf(apiMenuCards + '?parentId=1&lang=' + i18nNames.ru, async () =>
			await Promise.resolve({ status: 200, body: JSON.stringify({ data }) })
		);
		fetchMock.mockOnceIf(apiMenuCards + '?parentId=1&lang=' + i18nNames.en, async () =>
			await Promise.resolve({ status: 200, body: JSON.stringify({ data }) })
		);
	});

	test(endpointName + ' [1 ru]', async () => {
		const { result } = renderHook(() => menuApi.useGetMenuCardsQuery({ parentId: 1, lang: i18nNames.ru }), { wrapper });

		// базовые тесты для всех RTKQuery хуков
		await RTKQueryApiExpect({ result, fetchMock, endpointName })

		// Получено не менее 1 записи
		expect(result.current.data!.length).toBeGreaterThanOrEqual(1)
		const resEl = result.current.data![0]

		// совпадает: ключи + очередность ключей
		expect(Object.keys(resEl)).toEqual(Object.values(dataKeys))

		// совпадает: ТИПЫ названий
		expect(typeof resEl.id).toBe('number')
		// expect(typeof resEl.parentId === 'number' || resEl.parentId === null).toBe(true)
		expect(typeof resEl.name === 'string' || resEl.name === null).toBe(true)
		expect(typeof resEl.url === 'string' || resEl.url === null).toBe(true)
	});

	test(endpointName + ' [1 en]', async () => {
		const { result } = renderHook(() => menuApi.useGetMenuCardsQuery({ parentId: 1, lang: i18nNames.en }), { wrapper });

		// базовые тесты для всех RTKQuery хуков
		await RTKQueryApiExpect({ result, fetchMock, endpointName })

		// Получено не менее 1 записи
		expect(result.current.data!.length).toBeGreaterThanOrEqual(1)
		const resEl = result.current.data![0]

		// совпадает: ключи + очередность ключей
		expect(Object.keys(resEl)).toEqual(Object.values(dataKeys))

		// совпадает: ТИПЫ названий
		expect(typeof resEl.id).toBe('number')
		// expect(typeof resEl.parentId === 'number' || resEl.parentId === null).toBe(true)
		expect(typeof resEl.name === 'string' || resEl.name === null).toBe(true)
		expect(typeof resEl.url === 'string' || resEl.url === null).toBe(true)
	});

	test(endpointName + ' null', async () => {
		const { result } = renderHook(() => menuApi.useGetMenuCardsQuery({}), { wrapper });

		// базовые тесты для всех RTKQuery хуков
		await RTKQueryApiExpect({ result, fetchMock, endpointName })

		// Получено не менее 1 записи
		expect(result.current.data!.length).toBeGreaterThanOrEqual(1)
		const resEl = result.current.data![0]

		// совпадает: ключи + очередность ключей
		expect(Object.keys(resEl)).toEqual(Object.values(dataKeys))

		// совпадает: ТИПЫ названий
		expect(typeof resEl.id).toBe('number')
		// expect(typeof resEl.parentId === 'number' || resEl.parentId === null).toBe(true)
		expect(typeof resEl.name === 'string' || resEl.name === null).toBe(true)
		expect(typeof resEl.url === 'string' || resEl.url === null).toBe(true)
	});
});
