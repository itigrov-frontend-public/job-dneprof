import { createSlice, type PayloadAction } from '@reduxjs/toolkit';
import { appStorage, appStorageList } from 'shared/components/appStorage';
import { type IMenuSlice } from 'shared/config/types/Menu';

const initialState: IMenuSlice = {
	open: appStorage.getItem(appStorageList.menu) === 'true'
}

export const menuSlice = createSlice({
	name: 'menu',
	initialState,
	reducers: {

		setMenuOpen (state, action: PayloadAction<boolean>) {
			state.open = action.payload;
			appStorage.setItem(appStorageList.menu, String(state.open))
			// localStorage.setItem(LOCAL_STORAGE_MENU_KEY, String(state.open));
		},

		toggleMenuOpen (state) {
			if (state.open) {
				state.open = false
			} else {
				state.open = true
			}
			appStorage.setItem(appStorageList.menu, String(state.open))
			// localStorage.setItem(LOCAL_STORAGE_MENU_KEY, String(state.open));
		}

	}

});

export const menuSliceReducer = menuSlice.reducer;
export const menuSliceActions = menuSlice.actions;
