import { appApi } from 'shared/components/appApi';
import { apiMenuCards } from 'shared/config/store/api';
import { i18nNames } from 'shared/config/types/i18n';
import { type IMenuCard } from 'shared/config/types/Menu';

interface getCardsProps {
	parentId?: number | null,
	lang?: i18nNames
}

const menuApi = appApi.injectEndpoints({
	endpoints: (builder) => ({
		// getCards: builder.query<IMenuCard[], void>({
		getMenuCards: builder.query<IMenuCard[], getCardsProps>({
			query: ({ parentId = null, lang = i18nNames.ru }) => ({
				url: apiMenuCards,
				params: { parentId, lang }
			})
		})

	}),
	overrideExisting: false
});
export { menuApi };
