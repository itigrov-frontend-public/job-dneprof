import { type FC, lazy, memo, Suspense } from 'react'
import Loader from 'shared/ui/Loader';

const MenuPcLazy = lazy<FC>(async () => await import('./MenuPc'))

const MenuPcWrapper: FC = () => {
	// __IS_DEV__ && console.log('feature/MenuPcWrapper');

	return (
		<Suspense fallback={<Loader/>}>
			{<MenuPcLazy/>}
		</Suspense>
	)
}

export default memo(MenuPcWrapper)
