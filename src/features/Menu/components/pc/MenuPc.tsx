import { type FC, memo } from 'react'
import { useLocation } from 'react-router-dom';

import getCards from '../../api/getMenuCards';
import MenuPcUi from '../../ui/MenuPcUi';

const MenuPc: FC = () => {
	const pathname = useLocation().pathname;
	const { data: cards, isError, isLoading } = getCards()

	// __IS_DEV__ && console.log('feature/menuPc');

	return (<MenuPcUi pathname={pathname} cards={cards} isError={isError} isLoading={isLoading} />)
}
export default memo(MenuPc)
