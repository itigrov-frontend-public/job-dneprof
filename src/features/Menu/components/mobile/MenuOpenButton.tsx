import React, { type FC, memo, useCallback } from 'react'
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { ButtonHamburger } from 'shared/ui/Button';

import { getMenuOpen } from '../../selectors/getMenuOpen';
import { menuSliceActions } from '../../store/menuSlice';

const MenuOpenButton: FC = () => {
	const dispatch = useDispatch();
	const open = useSelector(getMenuOpen)
	const { t } = useTranslation()

	const toggleMenuOpen = useCallback(() => {
		dispatch(menuSliceActions.toggleMenuOpen());
	}, [dispatch]);

	return (
		<ButtonHamburger open={open} onClick={toggleMenuOpen} title={t('button-MenuOpenButton')}/>
	)
}

export default memo(MenuOpenButton)
