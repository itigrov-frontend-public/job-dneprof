import { type FC, memo, useCallback } from 'react'
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import useAddStore from 'shared/hooks/useAddStore';

import { getMenuOpen } from '../../selectors/getMenuOpen';
import { menuSliceActions, menuSliceReducer } from '../../store/menuSlice';
import MenuMobileWrapperUi from '../../ui/MenuMobileWrapperUi';

const MenuMobileWrapper: FC = () => {
	useAddStore('menu', menuSliceReducer)
	const open = useSelector(getMenuOpen)
	const { t } = useTranslation()

	const dispatch = useDispatch();
	const closeOpen = useCallback(() => {
		dispatch(menuSliceActions.setMenuOpen(false));
	}, [dispatch]);

	// __IS_DEV__ && console.log('feature/MenuMobileWrapper');

	return (
		<MenuMobileWrapperUi open={open} onClick={closeOpen} titleClose={t('button-MenuOpenButton-close')} />
	)
}

export default memo(MenuMobileWrapper)
