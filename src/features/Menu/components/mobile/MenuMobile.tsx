import { type FC, memo } from 'react'
import { useLocation } from 'react-router-dom';

import getMenuCards from '../../api/getMenuCards';
import MenuMobileUi from '../../ui/MenuMobileUi';

const MenuMobile: FC = () => {
	const pathname = useLocation().pathname;
	const { data: cards, isError, isLoading } = getMenuCards()

	// __IS_DEV__ && console.log('feature/MenuMobile', isError, isLoading, cards);

	return (
		<MenuMobileUi
			pathname={pathname}
			cards={cards}
			isError={isError}
			isLoading={isLoading} />
	)
}

export default memo(MenuMobile)
