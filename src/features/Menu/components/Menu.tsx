import { type FC, memo } from 'react'
import { useSelector } from 'react-redux'
import { getPoints } from 'entities/Breakpoints'

import MenuMobileWrapper from './mobile/MenuMobileWrapper';
import MenuPcWrapper from './pc/MenuPcWrapper';

const Menu: FC = () => {
	const points = useSelector(getPoints)

	// __IS_DEV__ && console.log('feature/Menu');

	return points.isMobile
		? <MenuMobileWrapper />
		: <MenuPcWrapper/>
}

export default memo(Menu)
