import { type DeepPartial } from '@reduxjs/toolkit';
import { type StateSchema } from 'shared/config/store/AppStore';

import { getMenuOpen } from './getMenuOpen';

describe('getMenuOpen', () => {
	test('getOpen: true', () => {
		const state: DeepPartial<StateSchema> = {
			menu: { open: true }
		};
		expect(getMenuOpen(state as StateSchema)).toBe(true);
	});

	test('getOpen: false', () => {
		const state: DeepPartial<StateSchema> = {
			menu: { open: false }
		};
		expect(getMenuOpen(state as StateSchema)).toBe(false);
	});

	test('getOpen: boolean(undefined)', () => {
		const state: DeepPartial<StateSchema> = { };
		expect(getMenuOpen(state as StateSchema)).toBe(false);
	});
});
