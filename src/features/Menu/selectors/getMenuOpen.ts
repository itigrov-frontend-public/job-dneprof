import { type StateSchema } from 'shared/config/store/AppStore';

export const getMenuOpen = (state: StateSchema) => Boolean(state.menu?.open);
