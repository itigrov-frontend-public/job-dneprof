
import type { Meta, StoryObj } from '@storybook/react';
import { i18nNames } from 'shared/config/types/i18n';
// import { i18nNames } from 'shared/config/types/i18n';
import { themeNames } from 'shared/config/types/Theme';
import { I18nDecorator, ThemeDecoratorStory } from 'shared/devTests/storybook';

import MenuPcWrapper from './components/pc/MenuPcWrapper';

const meta = {
	title: 'features/menu/PC',
	component: MenuPcWrapper,
	argTypes: {},
	args: {},
	parameters: {
		backgrounds: {
			default: 'dark'
		},
		layout: 'centered' // centered | fullscreen | undefined
	}

} satisfies Meta<typeof MenuPcWrapper>;

export default meta;

type Story = StoryObj<typeof meta>;

export const MenuPcAll: Story = { args: { open: true } };
MenuPcAll.decorators = [
	(Story, context) => ThemeDecoratorStory(Story, context),
	(Story, context) => I18nDecorator(Story, context)
];

export const MenuPcEnDark: Story = { args: { open: true } };
MenuPcEnDark.decorators = [
	(Story, context) => ThemeDecoratorStory(Story, context, themeNames.DARK),
	(Story, context) => I18nDecorator(Story, context, i18nNames.en)
];
