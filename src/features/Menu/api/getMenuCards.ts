import { getLangTyped } from 'entities/i18n'

import { menuApi } from '../store/menuApi'

const getMenuCards = (parentId: number | undefined = 1) => {
	const lang = getLangTyped();

	return menuApi.useGetMenuCardsQuery({
		parentId,
		lang
	})
}

export default getMenuCards
