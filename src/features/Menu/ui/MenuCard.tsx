import React, { type FC, memo } from 'react'
import { Link } from 'react-router-dom'
import { type IMenuCard } from 'shared/config/types/Menu'

interface MenuCardProps { // extends LinkProps {
	card: IMenuCard
	cnCurrent?: string
	onClick?: () => void
}
const MenuCard: FC<MenuCardProps> = ({ card, cnCurrent, ...otherProps }) => {
	// __IS_DEV__ && console.log('feature/menuCard', card.name);

	return (
		<li>
			<Link
				to={card.url}
				className={cnCurrent}
				{...otherProps}
			>
				{card.name}
			</Link>
		</li>
	)
}

export default memo(MenuCard)
