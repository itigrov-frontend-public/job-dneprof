import { type FC, memo } from 'react'
import { type IMenuCard } from 'shared/config/types/Menu';
import { classNames } from 'shared/helpers/classNames';
import ErrorLoading from 'shared/ui/ErrorLoading'
import Loader from 'shared/ui/Loader'

import MenuCard from './MenuCard'

import '../style/menu.scss'
import cn from '../style/menuPc.module.scss';

interface MenuPcUiProps {
	cards?: IMenuCard[],
	isLoading: boolean,
	isError: boolean,
	pathname: string
}

const MenuPcUi: FC<MenuPcUiProps> = ({ cards, isLoading, isError, pathname }) => {
	// __IS_DEV__ && console.log('feature/menuPcUi');

	return (
		<nav className={classNames(cn.menu, {}, ['menu'])}>
			<ul>
				{isLoading && <Loader />}
				{isError && <ErrorLoading/>}
				{cards?.map(card =>
					<MenuCard
						key={card.id}
						card={card}
						cnCurrent={pathname === card.url ? cn.active : ''}
					/>
				)}
			</ul>
		</nav>
	)
}
export default memo(MenuPcUi)
