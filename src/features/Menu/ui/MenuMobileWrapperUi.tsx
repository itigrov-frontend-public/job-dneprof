import { type FC, lazy, memo, Suspense } from 'react'
import { classNames } from 'shared/helpers/classNames';
import { PortalProvider } from 'shared/providers/PortalProvider';
import { ButtonHamburger } from 'shared/ui/Button';
import Loader from 'shared/ui/Loader';

import '../style/menu.scss'
import cn from '../style/menuMobile.module.scss';

const MenuMobileLazy = lazy<FC>(async () => await import('../components/mobile/MenuMobile'))

const MenuMobileWrapperUi: FC<{ open: boolean, onClick: () => void, titleClose: string }> = ({ open, onClick, titleClose }) => {
	return (
		<PortalProvider>
			<nav className={classNames(cn.menuMobile, { [cn.menuOpen]: open }, [])}>
				<div className={cn.top}>
					<ButtonHamburger open={true} onClick={onClick} title={titleClose} />
				</div>
				<Suspense fallback={<Loader/>}>
					{open && <MenuMobileLazy/>}
				</Suspense>
			</nav>
			<div
				className={classNames(cn.shadow, { [cn.shadowOpen]: open }, ['menu'])}
				onClick={onClick}
			></div>
		</PortalProvider>
	)
}

export default memo(MenuMobileWrapperUi)
