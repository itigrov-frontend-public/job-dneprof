import { type FC, memo, useCallback } from 'react'
import { useDispatch } from 'react-redux';
import { ChangeLangButton } from 'entities/i18n';
import { ChangeThemeButton } from 'entities/Theme';
import { type IMenuCard } from 'shared/config/types/Menu';
import ErrorLoading from 'shared/ui/ErrorLoading';
import Loader from 'shared/ui/Loader';

import { menuSliceActions } from '../store/menuSlice';

import MenuCard from './MenuCard'

import cn from '../style/menuMobile.module.scss';

interface MenuMobileUiProps {
	cards?: IMenuCard[],
	isLoading: boolean,
	isError: boolean,
	pathname: string
}

const MenuMobileUi: FC<MenuMobileUiProps> = ({ cards, isLoading, isError, pathname }) => {
	const dispatch = useDispatch();
	const clickMenuCard = useCallback(() => {
		dispatch(menuSliceActions.setMenuOpen(false));
	}, [dispatch]);

	// __IS_DEV__ && console.log('feature/MenuMobile UI', isError, isLoading, cards);

	return (
		<>
			<ul>
				{isLoading && <Loader />}
				{isError && <ErrorLoading/>}
				{cards?.map(card =>
					<MenuCard
						key={card.id}
						card={card}
						cnCurrent={pathname === card.url ? cn.active : ''}
						onClick={clickMenuCard}
					/>
				)}
			</ul>
			<div className={cn.bottom}>
				<ChangeThemeButton/>
				<ChangeLangButton/>
			</div>
		</>
	)
}

export default memo(MenuMobileUi)
