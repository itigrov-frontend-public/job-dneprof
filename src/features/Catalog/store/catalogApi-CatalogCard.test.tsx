import { renderHook } from '@testing-library/react';
import { apiCatalogCard } from 'shared/config/store/api';
import { i18nNames } from 'shared/config/types/i18n';
import { fetchMock, RTKQueryApiExpect, StoreDecorator } from 'shared/devTests/unit';

import { catalogApi } from './catalogApi';

const wrapper = StoreDecorator;

// useGetCardsQuery
describe('catalogApi: useGetCatalogCardQuery', () => {
	const data = {}
	const endpointName = 'getCatalogCard';
	const dataKeys = ['id', 'huu', 'name', 'notice', 'text', 'price', 'priceOld', 'priceType', 'avito', 'group', 'images']

	beforeEach(() => {
		fetchMock.resetMocks();
		fetchMock.mockOnceIf(apiCatalogCard + '?groupHuu=accessories&huu=1&lang=' + i18nNames.ru, async () =>
			await Promise.resolve({ status: 200, body: JSON.stringify({ data }) })
		);
		fetchMock.mockOnceIf(apiCatalogCard + '?groupHuu=accessories&huu=1&lang=' + i18nNames.en, async () =>
			await Promise.resolve({ status: 200, body: JSON.stringify({ data }) })
		);
	});

	// BEGIN getCatalogCard
	test(endpointName + ' [huu ru]', async () => {
		const { result } = renderHook(() => catalogApi.useGetCatalogCardQuery({ groupHuu: 'accessories', huu: '1', lang: i18nNames.ru }), { wrapper });

		// базовые тесты для всех RTKQuery хуков
		await RTKQueryApiExpect({ result, fetchMock, endpointName })

		// Получено не менее 1 записи
		// expect(result.current.data!.length).toBeGreaterThanOrEqual(1)
		// const resEl = result.current.data![0]
		const resEl = result.current.data!

		// совпадает: ключи + очередность ключей
		expect(Object.keys(resEl)).toEqual(Object.values(dataKeys))

		// совпадает: ТИПЫ названий
		expect(typeof resEl.id).toBe('number')
		expect(typeof resEl.huu === 'string' || resEl.huu === null).toBe(true)
		expect(typeof resEl.name === 'string' || resEl.name === null).toBe(true)
		expect(typeof resEl.notice === 'string' || resEl.notice === null).toBe(true)
		expect(typeof resEl.text === 'string' || resEl.text === null).toBe(true)
		expect(typeof resEl.price === 'number' || resEl.price === null).toBe(true)
		expect(typeof resEl.priceOld === 'number' || resEl.priceOld === null).toBe(true)
		expect(typeof resEl.priceType === 'number' || resEl.priceType === null).toBe(true)
		expect(typeof resEl.group === 'object' || resEl.group === null).toBe(true)
		expect(typeof resEl.images === 'object' || resEl.images === null).toBe(true)
	});

	test(endpointName + ' [huu en]', async () => {
		const { result } = renderHook(() => catalogApi.useGetCatalogCardQuery({ groupHuu: 'accessories', huu: '1', lang: i18nNames.en }), { wrapper });

		// базовые тесты для всех RTKQuery хуков
		await RTKQueryApiExpect({ result, fetchMock, endpointName })

		// Получено не менее 1 записи
		// expect(result.current.data!.length).toBeGreaterThanOrEqual(1)
		// const resEl = result.current.data![0]
		const resEl = result.current.data!

		// совпадает: ключи + очередность ключей
		expect(Object.keys(resEl)).toEqual(Object.values(dataKeys))

		// совпадает: ТИПЫ названий
		expect(typeof resEl.id).toBe('number')
		expect(typeof resEl.huu === 'string' || resEl.huu === null).toBe(true)
		expect(typeof resEl.name === 'string' || resEl.name === null).toBe(true)
		expect(typeof resEl.notice === 'string' || resEl.notice === null).toBe(true)
		expect(typeof resEl.text === 'string' || resEl.text === null).toBe(true)
		expect(typeof resEl.price === 'number' || resEl.price === null).toBe(true)
		expect(typeof resEl.priceOld === 'number' || resEl.priceOld === null).toBe(true)
		expect(typeof resEl.priceType === 'number' || resEl.priceType === null).toBe(true)
		expect(typeof resEl.group === 'object' || resEl.group === null).toBe(true)
		expect(typeof resEl.images === 'object' || resEl.images === null).toBe(true)
	});

	/*
	test(endpointName + ' [undefined en]', async () => {
		const { result } = renderHook(() => catalogApi.useGetCatalogCardQuery({ huu: undefined, lang: i18nNames.en }), { wrapper });

		// базовые тесты для всех RTKQuery хуков
		await RTKQueryApiExpect({ result, fetchMock, endpointName })

		// Получено не менее 1 записи
		// expect(result.current.data!.length).toBeGreaterThanOrEqual(1)
		// const resEl = result.current.data![0]
		const resEl = result.current.data!

		// совпадает: ключи + очередность ключей
		expect(Object.keys(resEl)).toEqual(Object.values(dataKeys))

		// совпадает: ТИПЫ названий
		expect(typeof resEl.id).toBe('number')
		expect(typeof resEl.huu === 'string' || resEl.huu === null).toBe(true)
		expect(typeof resEl.name === 'string' || resEl.name === null).toBe(true)
		expect(typeof resEl.notice === 'string' || resEl.notice === null).toBe(true)
		expect(typeof resEl.price === 'number' || resEl.price === null).toBe(true)
		expect(typeof resEl.priceOld === 'number' || resEl.priceOld === null).toBe(true)
		expect(typeof resEl.priceType === 'number' || resEl.priceType === null).toBe(true)
	});
	*/
	// END getCatalogCard
});
