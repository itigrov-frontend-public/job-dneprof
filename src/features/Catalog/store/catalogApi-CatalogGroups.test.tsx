import { renderHook } from '@testing-library/react';
import { apiCatalogGroups } from 'shared/config/store/api';
import { i18nNames } from 'shared/config/types/i18n';
import { fetchMock, RTKQueryApiExpect, StoreDecorator } from 'shared/devTests/unit';

import { catalogApi } from './catalogApi';

const wrapper = StoreDecorator;

// useGetCardsQuery
describe('catalogApi: useGetCatalogGroupsQuery', () => {
	const data = {}
	const endpointName = 'getCatalogGroups';
	const dataKeys = ['id', 'huu', 'name']

	beforeEach(() => {
		fetchMock.resetMocks();
		fetchMock.mockOnceIf(apiCatalogGroups + '?parentId=1&lang=' + i18nNames.ru, async () =>
			await Promise.resolve({ status: 200, body: JSON.stringify({ data }) })
		);
		fetchMock.mockOnceIf(apiCatalogGroups + '?parentId=1&lang=' + i18nNames.en, async () =>
			await Promise.resolve({ status: 200, body: JSON.stringify({ data }) })
		);
		fetchMock.mockOnceIf(apiCatalogGroups + '?parentHuu=-system-root&lang=' + i18nNames.ru, async () =>
			await Promise.resolve({ status: 200, body: JSON.stringify({ data }) })
		);
	});

	// BEGIN getCatalogGroups
	test(endpointName + ' [1 ru]', async () => {
		const { result } = renderHook(() => catalogApi.useGetCatalogGroupsQuery({ parentId: 1, lang: i18nNames.ru }), { wrapper });

		// базовые тесты для всех RTKQuery хуков
		await RTKQueryApiExpect({ result, fetchMock, endpointName })

		// console.log(result.current.data);

		// Получено не менее 1 записи
		expect(result.current.data!.length).toBeGreaterThanOrEqual(1)
		const resEl = result.current.data![0]

		// совпадает: ключи + очередность ключей
		expect(Object.keys(resEl)).toEqual(Object.values(dataKeys))

		// совпадает: ТИПЫ названий
		expect(typeof resEl.id).toBe('number')
		expect(typeof resEl.huu === 'string' || resEl.huu === null).toBe(true)
		expect(typeof resEl.name === 'string' || resEl.name === null).toBe(true)
	});

	test(endpointName + ' [1 en]', async () => {
		const { result } = renderHook(() => catalogApi.useGetCatalogGroupsQuery({ parentId: 1, lang: i18nNames.en }), { wrapper });

		// базовые тесты для всех RTKQuery хуков
		await RTKQueryApiExpect({ result, fetchMock, endpointName })

		// console.log(result.current.data);

		// Получено не менее 1 записи
		expect(result.current.data!.length).toBeGreaterThanOrEqual(1)
		const resEl = result.current.data![0]

		// совпадает: ключи + очередность ключей
		expect(Object.keys(resEl)).toEqual(Object.values(dataKeys))

		// совпадает: ТИПЫ названий
		expect(typeof resEl.id).toBe('number')
		expect(typeof resEl.huu === 'string' || resEl.huu === null).toBe(true)
		expect(typeof resEl.name === 'string' || resEl.name === null).toBe(true)
	});

	test(endpointName + ' [-system-root ru]', async () => {
		const { result } = renderHook(() => catalogApi.useGetCatalogGroupsQuery({ parentHuu: '-system-root', lang: i18nNames.ru }), { wrapper });

		// базовые тесты для всех RTKQuery хуков
		await RTKQueryApiExpect({ result, fetchMock, endpointName })

		// console.log(result.current.data);

		// Получено не менее 1 записи
		expect(result.current.data!.length).toBeGreaterThanOrEqual(1)
		const resEl = result.current.data![0]

		// совпадает: ключи + очередность ключей
		expect(Object.keys(resEl)).toEqual(Object.values(dataKeys))

		// совпадает: ТИПЫ названий
		expect(typeof resEl.id).toBe('number')
		expect(typeof resEl.huu === 'string' || resEl.huu === null).toBe(true)
		expect(typeof resEl.name === 'string' || resEl.name === null).toBe(true)
	});
	// END getCatalogGroups
});
