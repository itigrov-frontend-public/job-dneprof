import { appApi } from 'shared/components/appApi';
import { apiCatalogCard, apiCatalogCards, apiCatalogGroup, apiCatalogGroups } from 'shared/config/store/api';
import { CATALOG_CARDS_LIMIT_DEFAULT, type ICatalogCard, type ICatalogCardPage, type ICatalogGroup, type ICatalogGroupPage } from 'shared/config/types/Catalog';
import { i18nNames } from 'shared/config/types/i18n';

interface getGroupProps {
	huu?: string
	lang?: i18nNames
}

interface getGroupsProps {
	parentHuu?: string
	parentId?: number
	lang?: i18nNames
}

interface getCardsProps {
	lang?: i18nNames
	groupHuu?: string
	groupId?: number
	onIndex?: number
	limit?: number
	page?: number
	search?: string
	favourite?: number[]
}

interface getCardProps {
	lang?: i18nNames
	huu?: string
	groupHuu?: string
}

const catalogApi = appApi.injectEndpoints({
	endpoints: (builder) => ({

		getCatalogGroup: builder.query<ICatalogGroupPage, getGroupProps>({
			query: ({ huu = '-system-root', lang = i18nNames.ru }) => ({
				url: apiCatalogGroup,
				params: { huu, lang }
			})
		}),

		getCatalogGroups: builder.query<ICatalogGroup[], getGroupsProps>({
			query: ({ parentHuu, parentId = 1, lang = i18nNames.ru }) => ({
				url: apiCatalogGroups,
				params: { parentHuu, parentId, lang }
			})
		}),

		getCatalogCards: builder.query<ICatalogCard[], getCardsProps>({
			query: ({
				lang = i18nNames.ru,
				groupHuu,
				groupId,
				onIndex,
				limit = CATALOG_CARDS_LIMIT_DEFAULT,
				page,
				search,
				favourite
			}) => ({
				url: apiCatalogCards,
				params: {
					lang,
					groupHuu,
					groupId,
					onIndex,
					limit,
					page,
					search,
					favourite
				}
			})
		}),

		getCatalogCard: builder.query<ICatalogCardPage, getCardProps>({
			query: ({ huu, groupHuu, lang = i18nNames.ru }) => ({
				url: apiCatalogCard,
				params: { huu, groupHuu, lang }
			})
		})

	}),
	overrideExisting: false
});
export { catalogApi };
