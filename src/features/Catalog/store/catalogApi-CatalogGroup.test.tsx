import { renderHook } from '@testing-library/react';
import { apiCatalogGroup } from 'shared/config/store/api';
import { i18nNames } from 'shared/config/types/i18n';
import { fetchMock, RTKQueryApiExpect, StoreDecorator } from 'shared/devTests/unit';

import { catalogApi } from './catalogApi';

const wrapper = StoreDecorator;

// useGetCardsQuery
describe('catalogApi: useGetCatalogGroupQuery', () => {
	const data = {}
	const endpointName = 'getCatalogGroup';
	const dataKeys = ['id', 'huu', 'name', 'name2']

	beforeEach(() => {
		fetchMock.resetMocks();
		fetchMock.mockOnceIf(apiCatalogGroup + '?huu=-system-root&lang=' + i18nNames.ru, async () =>
			await Promise.resolve({ status: 200, body: JSON.stringify({ data }) })
		);
		fetchMock.mockOnceIf(apiCatalogGroup + '?huu=-system-root&lang=' + i18nNames.en, async () =>
			await Promise.resolve({ status: 200, body: JSON.stringify({ data }) })
		);
	});

	// BEGIN getCatalogGroup
	test(endpointName + ' [huu ru]', async () => {
		const { result } = renderHook(() => catalogApi.useGetCatalogGroupQuery({ huu: '-system-root', lang: i18nNames.ru }), { wrapper });

		// базовые тесты для всех RTKQuery хуков
		await RTKQueryApiExpect({ result, fetchMock, endpointName })

		// console.log(result.current.data);

		// Получено не менее 1 записи
		// expect(result.current.data!.length).toBeGreaterThanOrEqual(1)
		// const resEl = result.current.data![0]
		const resEl = result.current.data!

		// совпадает: ключи + очередность ключей
		expect(Object.keys(resEl)).toEqual(Object.values(dataKeys))

		// совпадает: ТИПЫ названий
		expect(typeof resEl.id).toBe('number')
		expect(typeof resEl.huu === 'string' || resEl.huu === null).toBe(true)
		expect(typeof resEl.name === 'string' || resEl.name === null).toBe(true)
		expect(typeof resEl.name2 === 'string' || resEl.name2 === null).toBe(true)
	});

	test(endpointName + ' [huu en]', async () => {
		const { result } = renderHook(() => catalogApi.useGetCatalogGroupQuery({ huu: '-system-root', lang: i18nNames.en }), { wrapper });

		// базовые тесты для всех RTKQuery хуков
		await RTKQueryApiExpect({ result, fetchMock, endpointName })

		// console.log(result.current.data);

		// Получено не менее 1 записи
		// expect(result.current.data!.length).toBeGreaterThanOrEqual(1)
		// const resEl = result.current.data![0]
		const resEl = result.current.data!

		// совпадает: ключи + очередность ключей
		expect(Object.keys(resEl)).toEqual(Object.values(dataKeys))

		// совпадает: ТИПЫ названий
		expect(typeof resEl.id).toBe('number')
		expect(typeof resEl.huu === 'string' || resEl.huu === null).toBe(true)
		expect(typeof resEl.name === 'string' || resEl.name === null).toBe(true)
		expect(typeof resEl.name2 === 'string' || resEl.name2 === null).toBe(true)
	});

	test(endpointName + ' [undefined en]', async () => {
		const { result } = renderHook(() => catalogApi.useGetCatalogGroupQuery({ huu: undefined, lang: i18nNames.en }), { wrapper });

		// базовые тесты для всех RTKQuery хуков
		await RTKQueryApiExpect({ result, fetchMock, endpointName })

		// console.log(result.current.data);

		// Получено не менее 1 записи
		// expect(result.current.data!.length).toBeGreaterThanOrEqual(1)
		// const resEl = result.current.data![0]
		const resEl = result.current.data!

		// совпадает: ключи + очередность ключей
		expect(Object.keys(resEl)).toEqual(Object.values(dataKeys))

		// совпадает: ТИПЫ названий
		expect(typeof resEl.id).toBe('number')
		expect(typeof resEl.huu === 'string' || resEl.huu === null).toBe(true)
		expect(typeof resEl.name === 'string' || resEl.name === null).toBe(true)
		expect(typeof resEl.name2 === 'string' || resEl.name2 === null).toBe(true)
	});
	// END getCatalogGroup
});
