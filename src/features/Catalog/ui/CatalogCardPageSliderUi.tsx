import { type FC, memo, useMemo } from 'react';
import { useSelector } from 'react-redux';
import SliderSwiper from 'entities/SliderSwiper';
import { getAppParam } from 'shared/components/appParam';
import { type ICatalogCardPage } from 'shared/config/types/Catalog';
import { getImageUrl } from 'shared/helpers/getImageUrl';
import { SwiperSlide } from 'swiper/react';

interface CatalogCardPageSliderUiProps {
	pageCard: ICatalogCardPage,
}

const CatalogCardPageSliderUi: FC<CatalogCardPageSliderUiProps> = ({ pageCard }) => {
	const appParam = useSelector(getAppParam)

	const gallarySlides = useMemo(() => {
		return pageCard.images.map((img) => {
			return (
				<SwiperSlide key={`swiperSlides-${img.id}`}>
					<img
						loading="lazy"
						src={getImageUrl('/catalogCards-list/', img.image, appParam.isWebp, appParam.isRetina)}
						alt={img.name || pageCard.name}
						title={img.name || pageCard.name}
						width={490}
						height={368}
					/>
				</SwiperSlide>
			)
		});
	}, [pageCard.id]);

	const gallaryThumbs = useMemo(() => {
		return pageCard.images.map((img) => {
			return (
				<SwiperSlide key={`swiperSlides-${img.id}`}>
					<img
						loading="lazy"
						src={getImageUrl('/catalogCards-list/', img.image, appParam.isWebp, appParam.isRetina)}
						alt={img.name || pageCard.name}
						title={img.name || pageCard.name}
						width={490}
						height={368}
					/>
				</SwiperSlide>
			)
		});
	}, [pageCard.id]);

	// __IS_DEV__ && console.log('feature/CatalogCardPageSliderUi LAZY');

	return (<SliderSwiper slides={gallarySlides} thumbs={gallaryThumbs} />)
}

export default memo(CatalogCardPageSliderUi)
