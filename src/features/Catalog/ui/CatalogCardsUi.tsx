import { type FC, memo, useCallback } from 'react'
import { useTranslation } from 'react-i18next'
import { useDispatch, useSelector } from 'react-redux'
import { checkFavourite, favouriteSliceActions, getFavouriteIds } from 'entities/Favourite'
import { type ICatalogCard } from 'shared/config/types/Catalog'

import CatalogCardUi from './CatalogCardUi'

import cn from '../styles/catalogCard.module.scss'

export interface CatalogCardsUiProps {
	cards: ICatalogCard[],
}

const CatalogCardsUi: FC<CatalogCardsUiProps> = ({ cards }) => {
	const favIds = useSelector(getFavouriteIds)
	const dispatch = useDispatch()

	const { t } = useTranslation()

	const onclickFavourite = useCallback((id: number) => {
		dispatch(favouriteSliceActions.toggleFavourite(id))
	}, [dispatch]);

	// __IS_DEV__ && console.log('feature/CatalogCardsUi', cards);

	return (
		<>
			<div className={cn.cards}>
				{cards.map(card =>
					<CatalogCardUi
						key={card.id}
						card={card}
						onclickFavourite={onclickFavourite}
						active={checkFavourite(favIds, card.id)}
						titleFavourite={t('button-addRemoveFavourite')}
					></CatalogCardUi>
				)}
			</div>
		</>
	)
}

export default memo(CatalogCardsUi)
