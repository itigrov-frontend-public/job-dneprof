import { type FC, lazy, Suspense } from 'react'
import { useTranslation } from 'react-i18next';
import { type ICatalogCardPage } from 'shared/config/types/Catalog';
import { formatPrice } from 'shared/helpers/format/formatPrice';
import { Button } from 'shared/ui/Button';
import Loader from 'shared/ui/Loader';

import '../../../shared/config/styles/components/content.sass'

import { catalogPriceType } from '../helpers/catalogPriceType';

import cn from '../styles/catalogCardContent.module.scss'

const CatalogCardPageSliderUi = lazy(async () => await import('./CatalogCardPageSliderUi'))

interface CatalogCardPageProps {
	pageCard: ICatalogCardPage,
}

const CatalogCardPageContentUi: FC<CatalogCardPageProps> = ({ pageCard }) => {
	const { t } = useTranslation()
	/*
	const appParam = useSelector(getAppParam)

	const gallarySlides = useMemo(() => {
		return pageCard.images.map((img) => {
			return (
				<SwiperSlide key={`swiperSlides-${img.id}`}>
					<img
						loading="lazy"
						src={getImageUrl('/catalogCards-list/', img.image, appParam.isWebp, appParam.isRetina)}
						alt={img.name || pageCard.name}
						title={img.name || pageCard.name}
						width={490}
						height={368}
					/>
				</SwiperSlide>
			)
		});
	}, [pageCard.id]);

	const gallaryThumbs = useMemo(() => {
		return pageCard.images.map((img) => {
			return (
				<SwiperSlide key={`swiperSlides-${img.id}`}>
					<img
						// loading="lazy"
						src={getImageUrl('/catalogCards-list/', img.image, appParam.isWebp, appParam.isRetina)}
						alt={img.name || pageCard.name}
						title={img.name || pageCard.name}
						width={490}
						height={368}
					/>
				</SwiperSlide>
			)
		});
	}, [pageCard.id]);
*/
	// __IS_DEV__ && console.log('feature/ContentCard content Ui');

	console.log(pageCard);

	return (
		<>
			<div className="pageName">{pageCard.name}</div>

			<div className={cn.CatalogCardContent}>
				<div className={cn.CatalogCardContent_left}>

					<Suspense fallback={<Loader />}>
						<CatalogCardPageSliderUi pageCard={pageCard} />
						{/* <SliderSwiperGallary slides={gallarySlides} thumbs={gallaryThumbs} /> */}
					</Suspense>

					{pageCard.price && <div className={cn.cardBottom}>
						<div className={cn.cardPriceType}>{catalogPriceType(pageCard.priceType)}</div>
						<div className={cn.cardPriceAll}>
							{pageCard.priceOld && <div className={cn.cardPriceOld}>{formatPrice(pageCard.priceOld)}</div>}
							<div className={cn.cardPriceNew}>{formatPrice(pageCard.price)}</div>
						</div>
					</div>}

				</div>
				<div className={cn.CatalogCardContent_right}>
					{pageCard?.text && <div dangerouslySetInnerHTML={{ __html: pageCard.text }}></div> }
					{pageCard.avito && <a href={pageCard.avito} target='_blank' rel="noreferrer"><Button>{t('buy-avito')}</Button></a>}
				</div>
			</div>

		</>
	)
}

export default CatalogCardPageContentUi
