import React, { type FC, memo } from 'react'
import { type ICatalogGroup } from 'shared/config/types/Catalog'
import { classNames } from 'shared/helpers/classNames'

import CatalogGroupUi from './CatalogGroupUi'

import cn from '../styles/catalogGroup.module.scss'

interface CatalogGroupsUiProps {
	groups: ICatalogGroup[],
	currentHuu?: string,
}

const CatalogGroupsUi: FC<CatalogGroupsUiProps> = ({ groups, currentHuu }) => {
	// __IS_DEV__ && console.log('feature/CatalogGroupsUi', currentHuu, groups);

	return (
		<>
			<div className={ classNames(cn.groups, {}, [])}>
				{groups.map(card =>
					<CatalogGroupUi key={card.id} card={card} currentHuu={currentHuu}></CatalogGroupUi>
				)}
			</div>
		</>
	)
}

export default memo(CatalogGroupsUi)
