import { type FC, memo } from 'react'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { getAppParam } from 'shared/components/appParam'
import { type ICatalogCard } from 'shared/config/types/Catalog'
import { classNames } from 'shared/helpers/classNames'
import { formatPrice } from 'shared/helpers/format/formatPrice'
import { getImageUrl } from 'shared/helpers/getImageUrl'
import SvgHeart from 'shared/static/svg/icons/heart.svg'
import { Icon } from 'shared/ui/Icon'

import { catalogPriceType } from '../helpers/catalogPriceType'

import cn from '../styles/catalogCard.module.scss'
interface CatalogCardUiProps {
	card: ICatalogCard,
	onclickFavourite: (id: number) => void
	active?: boolean
	titleFavourite?: string
}

const CatalogCardUi: FC<CatalogCardUiProps> = ({ card, onclickFavourite, active = false, titleFavourite = '' }) => {
	const to = '/catalog/' + card.group.huu + '/' + card.huu;
	const cardImage = card.image ? card.image[0] : null;
	const appParam = useSelector(getAppParam)
	const imageUrl = getImageUrl('/catalogCards-list', cardImage?.image, appParam.isWebp, appParam.isRetina)

	// __IS_DEV__ && console.log('feature/CatalogCardUi');

	return (
		<>
			<div className={classNames(cn.cardItem)} >

				<div className={cn.cardTop}>
					<div className={cn.cardImage}>
						<Icon
							title={titleFavourite}
							key={card.id}
							className={classNames(cn.fav, { [cn.favActive]: active, [cn.favNotMobile]: !appParam.isMobile }, [cn.icon])}
							onClick={() => { onclickFavourite(card.id); }}
						><SvgHeart/></Icon>

						<Link to={to}>
							<img
								src={imageUrl}
								alt={cardImage?.name || card.name}
								title={cardImage?.name || card.name}
								width={490}
								height={368}
							/>
						</Link>
					</div>
					{card.name && <div className={cn.cardName}><Link to={to}>{card.name}</Link></div>}
					{card.notice && <div className={cn.cardNotice}>{card.notice}</div>}
				</div>

				{card.price && <div className={cn.cardBottom}>
					<div className={cn.cardPriceType}>{catalogPriceType(card.priceType)}</div>
					<div className={cn.cardPriceAll}>
						{card.priceOld && <div className={cn.cardPriceOld}>{formatPrice(card.priceOld)}</div>}
						<div className={cn.cardPriceNew}>{formatPrice(card.price)}</div>
					</div>
				</div>}

			</div>
		</>
	)
}

export default memo(CatalogCardUi)
