import React, { type FC, memo } from 'react'
import { Link } from 'react-router-dom'
import { type ICatalogGroup } from 'shared/config/types/Catalog'
import { classNames } from 'shared/helpers/classNames'

import cn from '../styles/catalogGroup.module.scss'

interface CatalogGroupUiProps {
	card: ICatalogGroup,
	currentHuu?: string
}

const CatalogGroupUi: FC<CatalogGroupUiProps> = ({ card, currentHuu }) => {
	return (
		<>
			<Link
				to={'/catalog/' + card.huu}
				className={classNames(cn.groups_item, { [cn.active]: currentHuu === card.huu })}
			>{card.name}</Link>
		</>
	)
}

export default memo(CatalogGroupUi)
