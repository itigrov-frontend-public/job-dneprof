import { type FC, memo } from 'react'
import { type ICatalogCardPage } from 'shared/config/types/Catalog';
import { classNames } from 'shared/helpers/classNames';
import ErrorLoading from 'shared/ui/ErrorLoading';
import Loader from 'shared/ui/Loader';

import CatalogCardPageContentUi from './CatalogCardContentUi';

interface ContentCardProps {
	pageCard?: ICatalogCardPage,
	isLoading: boolean,
	isError: boolean,
}

const CatalogCardPageUi: FC<ContentCardProps> = ({ pageCard, isLoading, isError }) => {
	// __IS_DEV__ && console.log('feature/ContentCard Ui');

	return (
		<>
			<section className={classNames('maxWidth mainSection content', {}, [])} >
				<div className="mainSection_in">
					{isLoading && <Loader />}
					{isError && <ErrorLoading/>}
					{pageCard &&
						<CatalogCardPageContentUi pageCard={pageCard}></CatalogCardPageContentUi>
					}
				</div>
			</section>
		</>
	)
}

export default memo(CatalogCardPageUi)
