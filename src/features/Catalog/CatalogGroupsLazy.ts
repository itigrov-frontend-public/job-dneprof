import { type FC, lazy } from 'react';

import type { CatalogGroupsProps } from './components/CatalogGroups';

export const CatalogGroupsLazy = lazy<FC<CatalogGroupsProps>>(async () => await import('./components/CatalogGroups'))
