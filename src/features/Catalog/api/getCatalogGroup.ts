
import { getLangTyped } from 'entities/i18n'

import { catalogApi } from '../store/catalogApi'

// interface getGroupProps {
// huu?: string,
// groups?: boolean,
// cards?: boolean,
// cardsPage?: number,
// cardsLimit?: number,
// }

const getCatalogGroup = (huu: string | undefined) => {
	return catalogApi.useGetCatalogGroupQuery({
		huu,
		lang: getLangTyped()
		// groups,
		// cards,
		// cardsPage,
		// cardsLimit
	})
}

export default getCatalogGroup
