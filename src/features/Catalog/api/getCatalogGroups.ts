
import { getLangTyped } from 'entities/i18n'

import { catalogApi } from '../store/catalogApi'

interface getGroupsProps {
	parentHuu?: string
	parentId?: number
}

const getCatalogGroups = ({ parentHuu, parentId }: getGroupsProps) => {
	return catalogApi.useGetCatalogGroupsQuery({
		parentHuu,
		parentId,
		lang: getLangTyped()
	})
}

export default getCatalogGroups
