import { getLangTyped } from 'entities/i18n'

import { catalogApi } from '../store/catalogApi'

const getCatalogCard = (huu: string, groupHuu: string) => {
	return catalogApi.useGetCatalogCardQuery({
		huu,
		groupHuu,
		lang: getLangTyped()
	})
}

export default getCatalogCard
