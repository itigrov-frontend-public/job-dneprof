
import { getLangTyped } from 'entities/i18n'

import { catalogApi } from '../store/catalogApi'

interface getCardsProps {
	groupHuu?: string
	groupId?: number

	onIndex?: number
	limit?: number
	page?: number
	search?: string
	favourite?: number[]
}

const getCatalogCards = ({ groupHuu, groupId, onIndex, limit, page, search, favourite }: getCardsProps) => {
	return catalogApi.useGetCatalogCardsQuery({
		lang: getLangTyped(),
		groupHuu,
		groupId,
		onIndex,
		limit,
		page,
		search,
		favourite
	})
}

export default getCatalogCards
