import { type FC, memo } from 'react'
import CatalogGroupsUi from 'features/Catalog/ui/CatalogGroupsUi'
import ErrorLoading from 'shared/ui/ErrorLoading'
import Loader from 'shared/ui/Loader'

import getCatalogGroups from '../api/getCatalogGroups'

export interface CatalogGroupsProps {
	parentHuu?: string
	parentId?: number
	currentHuu?: string
}

const CatalogGroups: FC<CatalogGroupsProps> = ({ parentHuu, parentId, currentHuu }) => {
	const { data: groups, isLoading, isError } = getCatalogGroups({ parentHuu, parentId })

	return (
		<>
			{isLoading && <Loader />}
			{isError && <ErrorLoading/>}
			{groups && <CatalogGroupsUi groups={groups} currentHuu={currentHuu} />}
		</>
	)
}

export default memo(CatalogGroups)
