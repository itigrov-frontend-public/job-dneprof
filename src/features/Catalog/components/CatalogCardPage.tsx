import { type FC, memo } from 'react'

import getCatalogCard from '../api/getCatalogCard';
import CatalogCardPageUi from '../ui/CatalogCardPageUi';

const CatalogCardPage: FC<{ huu: string, groupHuu: string }> = ({ huu, groupHuu }) => {
	const { data: pageCard, isLoading, isError } = getCatalogCard(huu, groupHuu)

	// __IS_DEV__ && console.log('feature/ContentcardsCard');

	return (
		<CatalogCardPageUi pageCard={pageCard} isLoading={isLoading} isError={isError} />
	)
}

export default memo(CatalogCardPage)
