import { type FC, memo } from 'react'
import ErrorLoading from 'shared/ui/ErrorLoading'
import Loader from 'shared/ui/Loader'

import getCatalogCards from '../api/getCatalogCards'
import CatalogCardsUi from '../ui/CatalogCardsUi'

export interface CatalogCardsProps {
	groupHuu?: string
	groupId?: number

	onIndex?: number
	limit?: number
	page?: number
	search?: string
	favourite?: number[]

	noCardsText?: string
}

const CatalogCards: FC<CatalogCardsProps> = ({ groupHuu, groupId, onIndex, limit, page, search, favourite, noCardsText }) => {
	const { data: cards, isLoading, isError } = getCatalogCards({ groupHuu, groupId, onIndex, limit, page, search, favourite })

	// __IS_DEV__ && console.log('CatalogCards', isLoading, isError);

	return (
		<>
			<div className="mBotBox">
				{isLoading && <Loader />}
				{isError && <ErrorLoading/>}
				{cards && cards.length > 0 && <CatalogCardsUi cards={cards} />}
				{cards && cards.length === 0 && noCardsText && <p>{noCardsText}</p>}
			</div>
		</>
	)
}

export default memo(CatalogCards)
