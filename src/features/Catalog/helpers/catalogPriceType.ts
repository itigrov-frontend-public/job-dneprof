// import { useMemo } from 'react';

import { useTranslation } from 'react-i18next';

type catalogPriceTypePgops = (
	value: number,
) => string;

export const catalogPriceType: catalogPriceTypePgops = function (value) {
	const { t } = useTranslation()

	// return useMemo(() => {
	switch (value) {
		case 0:
			return ''
		case 1:
			return t('text-price-type1') + ':'
		case 2:
			return t('text-price-type2') + ':'
		case 3:
			return t('text-price-type3') + ':'
		default:
			return ''
	}
	// }, [value])
}
