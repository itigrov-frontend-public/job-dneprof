import { type FC, lazy } from 'react';

import type { CatalogCardsProps } from './components/CatalogCards';

export const CatalogCardsLazy = lazy<FC<CatalogCardsProps>>(async () => await import('./components/CatalogCards'))
