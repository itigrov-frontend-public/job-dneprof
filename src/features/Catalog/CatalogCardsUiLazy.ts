import { type FC, lazy } from 'react';

import type { CatalogCardsUiProps } from './ui/CatalogCardsUi';

export const CatalogCardsUiLazy = lazy<FC<CatalogCardsUiProps>>(async () => await import('./ui/CatalogCardsUi'))
