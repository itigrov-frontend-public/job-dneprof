import { type FC, memo } from 'react'
import { type IContentCard } from 'shared/config/types/Content';
import { classNames } from 'shared/helpers/classNames';
import ErrorLoading from 'shared/ui/ErrorLoading';
import Loader from 'shared/ui/Loader';

import ContentCardContentUi from './ContentCardContentUi';

interface ContentCardProps {
	pageCard?: IContentCard,
	isLoading: boolean,
	isError: boolean,
}

const ContentCardUi: FC<ContentCardProps> = ({ pageCard, isLoading, isError }) => {
	// __IS_DEV__ && console.log('feature/ContentCard Ui');

	return (
		<>
			{isLoading && <Loader />}
			{isError && <ErrorLoading/>}
			{pageCard &&
				<section className={classNames('maxWidth mainSection content', {}, [])} >
					<div className="mainSection_in pBotBox">
						<ContentCardContentUi pageCard={pageCard}></ContentCardContentUi>
					</div>
				</section>
			}
		</>
	)
}

export default memo(ContentCardUi)
