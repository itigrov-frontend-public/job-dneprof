import { type FC, memo } from 'react'
import { type IContentCard } from 'shared/config/types/Content';

import '../../../shared/config/styles/components/content.sass'

interface ContentCardProps {
	pageCard: IContentCard,
}

const ContentCardContentUi: FC<ContentCardProps> = ({ pageCard }) => {
	let text = '';
	if (pageCard?.text) { text = pageCard?.text }

	// __IS_DEV__ && console.log('feature/ContentCard content Ui');

	return (
		<>
			<div className="pageName">{pageCard.name}</div>
			<div dangerouslySetInnerHTML={{ __html: text }}></div>
		</>
	)
}

export default memo(ContentCardContentUi)
