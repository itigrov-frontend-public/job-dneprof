import { type FC, memo } from 'react'

import getContentCard from '../api/getContentCard';
import ContentCardUi from '../ui/ContentCardUi';

const ContentCard: FC<{ huu: string }> = ({ huu }) => {
	const { data: pageCard, isLoading, isError } = getContentCard(huu)

	// __IS_DEV__ && console.log('feacure/ContentCard');

	return (
		<ContentCardUi pageCard={pageCard} isLoading={isLoading} isError={isError} />
	)
}

export default memo(ContentCard)
