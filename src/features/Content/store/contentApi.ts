import { appApi } from 'shared/components/appApi';
import { apiContentCard } from 'shared/config/store/api';
import { type IContentCard } from 'shared/config/types/Content';
import { i18nNames } from 'shared/config/types/i18n';

interface getCardProps {
	huu: string
	lang?: i18nNames
}

const contentApi = appApi.injectEndpoints({
	endpoints: (builder) => ({
		// getCards: builder.query<IMenuCard[], void>({
		getContentCard: builder.query<IContentCard, getCardProps>({
			query: ({ huu, lang = i18nNames.ru }) => ({
				url: apiContentCard,
				params: { huu, lang }
			})
		})

	}),
	overrideExisting: false
});
export { contentApi };
