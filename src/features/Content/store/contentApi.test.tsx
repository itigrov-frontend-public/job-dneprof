import { renderHook } from '@testing-library/react';
import { apiContentCard } from 'shared/config/store/api';
import { i18nNames } from 'shared/config/types/i18n';
import { fetchMock, RTKQueryApiExpect, StoreDecorator } from 'shared/devTests/unit';

import { contentApi } from './contentApi';

const wrapper = StoreDecorator;

// useGetCardsQuery
describe('contentApi: useGetCardQuery', () => {
	const data = {}
	const endpointName = 'getContentCard';
	const dataKeys = ['id', 'huu', 'name', 'text']

	beforeEach(() => {
		fetchMock.resetMocks();
		fetchMock.mockOnceIf(apiContentCard + '?huu=delivery', async () =>
			await Promise.resolve({ status: 200, body: JSON.stringify({ data }) })
		);
		fetchMock.mockOnceIf(apiContentCard + '?huu=delivery&lang=' + i18nNames.ru, async () =>
			await Promise.resolve({ status: 200, body: JSON.stringify({ data }) })
		);
		fetchMock.mockOnceIf(apiContentCard + '?huu=delivery&lang=' + i18nNames.en, async () =>
			await Promise.resolve({ status: 200, body: JSON.stringify({ data }) })
		);
	});

	test(endpointName + ' [delivery ru]', async () => {
		const { result } = renderHook(() => contentApi.useGetContentCardQuery({ huu: 'delivery', lang: i18nNames.ru }), { wrapper });

		// базовые тесты для всех RTKQuery хуков
		await RTKQueryApiExpect({ result, fetchMock, endpointName })

		// Получено не менее 1 записи
		// expect(result.current.data!.length).toBeGreaterThanOrEqual(1)
		const resEl = result.current.data!

		// совпадает: ключи + очередность ключей
		expect(Object.keys(resEl)).toEqual(Object.values(dataKeys))

		// совпадает: ТИПЫ названий
		expect(typeof resEl.id).toBe('number')
		expect(typeof resEl.huu === 'string').toBe(true)
		expect(typeof resEl.name === 'string' || resEl.name === null).toBe(true)
		expect(typeof resEl.text === 'string' || resEl.text === null).toBe(true)
	});

	test(endpointName + ' [delivery en]', async () => {
		const { result } = renderHook(() => contentApi.useGetContentCardQuery({ huu: 'delivery', lang: i18nNames.en }), { wrapper });

		// базовые тесты для всех RTKQuery хуков
		await RTKQueryApiExpect({ result, fetchMock, endpointName })

		// Получено не менее 1 записи
		// expect(result.current.data!.length).toBeGreaterThanOrEqual(1)
		const resEl = result.current.data!

		// совпадает: ключи + очередность ключей
		expect(Object.keys(resEl)).toEqual(Object.values(dataKeys))

		// совпадает: ТИПЫ названий
		expect(typeof resEl.id).toBe('number')
		expect(typeof resEl.huu === 'string').toBe(true)
		expect(typeof resEl.name === 'string' || resEl.name === null).toBe(true)
		expect(typeof resEl.text === 'string' || resEl.text === null).toBe(true)
	});

	test(endpointName + ' [delivery]', async () => {
		const { result } = renderHook(() => contentApi.useGetContentCardQuery({ huu: 'delivery' }), { wrapper });

		// базовые тесты для всех RTKQuery хуков
		await RTKQueryApiExpect({ result, fetchMock, endpointName })

		// Получено не менее 1 записи
		// expect(result.current.data!.length).toBeGreaterThanOrEqual(1)
		const resEl = result.current.data!

		// совпадает: ключи + очередность ключей
		expect(Object.keys(resEl)).toEqual(Object.values(dataKeys))

		// совпадает: ТИПЫ названий
		expect(typeof resEl.id).toBe('number')
		expect(typeof resEl.huu === 'string').toBe(true)
		expect(typeof resEl.name === 'string' || resEl.name === null).toBe(true)
		expect(typeof resEl.text === 'string' || resEl.text === null).toBe(true)
	});
});
