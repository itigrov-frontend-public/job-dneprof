import getContentCard from './api/getContentCard'
import ContentCard from './components/ContentCard'
import ContentCardUi from './ui/ContentCardUi'

export {
	ContentCard,
	ContentCardUi,
	getContentCard
}
