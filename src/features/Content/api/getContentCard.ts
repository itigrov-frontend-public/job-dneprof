import { getLangTyped } from 'entities/i18n'

import { contentApi } from '../store/contentApi'

const getContentCard = (huu: string) => {
	return contentApi.useGetContentCardQuery({
		huu,
		lang: getLangTyped()
	})
}

export default getContentCard
