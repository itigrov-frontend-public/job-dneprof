export enum ContentErrorCode {
	error0 = 0,
	error404 = 404,
	error500 = 500,
}
