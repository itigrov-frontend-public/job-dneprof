import { type FC, memo } from 'react'
import { Trans, useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'

import 'shared/config/styles/components/content.sass'

const Error0: FC = () => {
	const { t } = useTranslation('error');
	const contentLink = <Link key={'link-/'} to="/" title={t('linkMainPage')}/>

	return (
		<>
			<p>
				<Link to='/' title={t('linkMainPage')}>
					<img src='/image/error.svg' width={390} height={340} alt={t('Error')} />
				</Link>
			</p>
			<p className="h1">{t('Error')}</p>
			<p>
				<Trans i18nKey={'error:content-0'} components={[contentLink]}>{t('error:content-0')}</Trans>
			</p>
		</>
	)
}

export default memo(Error0)
