import { type FC, memo, useMemo } from 'react'
import { classNames } from 'shared/helpers/classNames'

import { ContentErrorCode } from '../type/ContentError';

import Error0 from './0';
import Error404 from './404';
import Error500 from './500';

import cn from '../styles/styles.module.scss'

const ContentError: FC<{ code: ContentErrorCode }> = ({ code = ContentErrorCode.error0 }) => {
	const errorText = useMemo(() => {
		let errorText
		switch (code) {
			case ContentErrorCode.error404:
				errorText = <Error404 />
				break;
			case ContentErrorCode.error500:
				errorText = <Error500 />
				break;

			default:
				errorText = <Error0 />
				break;
		}
		return errorText
	}, [code])

	return (
		<section className={classNames('maxWidth mainSection', {}, [cn.errorPage])}>
			<article className={classNames(cn.errorPage_in, {}, ['content'])}>
				{errorText}
			</article>
		</section>
	)
}

export default memo(ContentError)
