import { type FC, memo } from 'react'
import { Trans, useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { Button } from 'shared/ui/Button'

import 'shared/config/styles/components/content.sass'

const Error500: FC = () => {
	const { t } = useTranslation('error');

	const reloadPage = () => {
		location.reload();
	};

	const contentButton = <Button key='buttonReloadPage' onClick={reloadPage}>{t('buttonReloadPage')}</Button>

	return (
		<>
			<p>
				<Link to='/' title={t('linkMainPage')}>
					<img src='/image/error.svg' width={390} height={340} alt={t('Error')} />
				</Link>
			</p>
			<p className="h1">{t('Error')} 500</p>
			<Trans i18nKey={'error:content-500'} components={[contentButton]}>{t('error:content-500')}</Trans>
		</>
	)
}

export default memo(Error500)
