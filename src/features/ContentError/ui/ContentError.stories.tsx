
import type { Meta, StoryObj } from '@storybook/react';
import { i18nNames } from 'shared/config/types/i18n';
import { themeNames } from 'shared/config/types/Theme';
import { I18nDecorator, ThemeDecoratorStory } from 'shared/devTests/storybook';

import { ContentErrorCode } from '../type/ContentError';

import ContentError from './ContentError';

const meta = {
	title: 'features/ContentError',
	component: ContentError,
	argTypes: {
	},
	args: {
		code: ContentErrorCode.error0
	},
	parameters: {
		layout: 'fullscreen' // centered | fullscreen | undefined
	}
} satisfies Meta<typeof ContentError>;

export default meta;

type Story = StoryObj<typeof meta>;

export const ContentErrorAll: Story = { };
ContentErrorAll.decorators = [
	(Story, context) => ThemeDecoratorStory(Story, context),
	(Story, context) => I18nDecorator(Story, context)
];

export const ContentError404RuDark: Story = { args: { code: ContentErrorCode.error404 } };
ContentError404RuDark.decorators = [
	(Story, context) => ThemeDecoratorStory(Story, context, themeNames.DARK),
	(Story, context) => I18nDecorator(Story, context, i18nNames.ru)
];

export const ContentError500EnLight: Story = { args: { code: ContentErrorCode.error500 } };
ContentError500EnLight.decorators = [
	(Story, context) => ThemeDecoratorStory(Story, context, themeNames.LIGHT),
	(Story, context) => I18nDecorator(Story, context, i18nNames.en)
];
