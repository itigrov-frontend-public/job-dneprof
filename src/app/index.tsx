import { type FC } from 'react'

import AppProvider from './providers/AppProvider'
import AppTemplate from './templates/AppTemplate'

const App: FC = () => {
	return (
		<AppProvider>
			<AppTemplate />
		</AppProvider>
	)
}

export default App
