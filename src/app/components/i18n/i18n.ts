import { initReactI18next } from 'react-i18next';
import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import Backend from 'i18next-http-backend';

void i18n
	.use(Backend)
	.use(LanguageDetector)
	.use(initReactI18next)
	.init({
		returnNull: false, // работает только через: i18next.d.ts

		fallbackLng: false, // 'ru'
		debug: false,
		// debug: __IS_DEV__,

		interpolation: {
			escapeValue: false // not needed for react as it escapes by default
		},
		// defaultNS: ['root'],
		backend: {
			loadPath: '/locales/{{lng}}/{{ns}}.json'
		}
	}, (err, t) => {
		if (err) { console.log('something went wrong loading i18n'); }
	})

export default i18n;
