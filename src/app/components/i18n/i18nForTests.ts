import { initReactI18next } from 'react-i18next';
import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import Backend from 'i18next-http-backend';

void i18n
	.use(Backend)
	.use(LanguageDetector)
	.use(initReactI18next)
	.init({
		// lng: 'ru',
		fallbackLng: false, // 'ru', // false,

		debug: true, // __IS_DEV__,
		returnNull: false, // работает только через: i18next.d.ts
		interpolation: {
			escapeValue: false
		},
		backend: {
			loadPath: '/locales/{{lng}}/{{ns}}.json'
		}
		// resources
	});

export default i18n;
