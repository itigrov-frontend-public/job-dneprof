import { type FC, type PropsWithChildren } from 'react'
import { useSelector } from 'react-redux';
import { getTheme, setThemeHelper } from 'entities/Theme';
import { type themeNames } from 'shared/config/types/Theme';

interface ThemeProviderProps extends PropsWithChildren {
	theme?: themeNames // need for storybook
}

const ThemeProvider: FC<ThemeProviderProps> = ({ children }) => {
	const theme = useSelector(getTheme)
	setThemeHelper(theme)

	return (<>{children}</>)
}

export default ThemeProvider
