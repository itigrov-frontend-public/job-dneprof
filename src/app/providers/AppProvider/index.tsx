import { type FC, type PropsWithChildren, StrictMode, Suspense } from 'react';
import { HelmetProvider } from 'react-helmet-async';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import AppParamProvider from 'app/providers//AppParamProvider';
import { AppError } from 'app/providers/AppError';
import store from 'app/providers/AppStore';
import BreakpointsProvider from 'app/providers/BreakpointsProvider';
import ThemeProvider from 'app/providers/ThemeProvider';
import Loader from 'shared/ui/Loader';

import 'app/components/i18n/i18n';

const AppProvider: FC<PropsWithChildren> = ({ children }) => {
	return (
		<StrictMode>
			<BrowserRouter>
				<AppError>
					<Suspense fallback={<Loader />}>
						<HelmetProvider>
							<Provider store={store}>
								<BreakpointsProvider>
									<AppParamProvider>
										<ThemeProvider>
											{children}
										</ThemeProvider>
									</AppParamProvider>
								</BreakpointsProvider>
							</Provider>
						</HelmetProvider>
					</Suspense>
				</AppError>
			</BrowserRouter>
		</StrictMode>
	)
}

export default AppProvider
