import { type CombinedState, configureStore, type Reducer, type ReducersMapObject } from '@reduxjs/toolkit'
import { createReducerManager } from 'app/components/store/ReducerManager';
import { breakpointsSliceReducer } from 'entities/Breakpoints';
import { favouriteSliceReducer } from 'entities/Favourite';
import { themeSliceReducer } from 'entities/Theme';
import { appApi } from 'shared/components/appApi';
import { appParamSliceReducer } from 'shared/components/appParam';
import { type IStore, type StateSchema } from 'shared/config/store/AppStore';

const staticReducers: ReducersMapObject<StateSchema> = {
	theme: themeSliceReducer,
	breakpoints: breakpointsSliceReducer,
	appParam: appParamSliceReducer,
	favourite: favouriteSliceReducer,

	[appApi.reducerPath]: appApi.reducer
}

// const rootReducer = combineReducers(staticReducers); // from '@reduxjs/toolkit'
const reducerManager = createReducerManager(staticReducers)

const initialState = {
	// reducer: rootReducer,
	reducer: reducerManager.reduce as Reducer<CombinedState<StateSchema>>,
	devTools: __IS_DEV__,
	// preloadedState?: StateSchema,
	middleware: (getDefaultMiddleware: () => any[]) => {
		return getDefaultMiddleware()
			.concat(appApi.middleware)
	}
}

const store = configureStore(initialState) as IStore;
store.reducerManager = reducerManager;
export default store

// store.reducerManager.add('theme777', themeSliceReducer);
// store.reducerManager.remove('theme777');
// export type AppDispatch = ReturnType<typeof store>['dispatch'];
