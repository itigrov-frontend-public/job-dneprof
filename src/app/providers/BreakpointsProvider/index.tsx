import { type FC, type PropsWithChildren } from 'react'
import { runBreakpoints } from 'entities/Breakpoints';

const BreakpointsProvider: FC<PropsWithChildren> = ({ children }) => {
	runBreakpoints()

	return (<>{children}</>)
}

export default BreakpointsProvider
