import { type FC, type PropsWithChildren } from 'react'
import { setAppParamMobile, setAppParamRetina, setAppParamScrollWidth, setAppParamWebp } from 'shared/components/appParam';

const AppParamProvider: FC<PropsWithChildren> = ({ children }) => {
	setAppParamWebp()
	setAppParamRetina()
	setAppParamMobile()
	setAppParamScrollWidth()

	return (<>{children}</>)
}

export default AppParamProvider
