import { useEffect, useState } from 'react';

// Компонент для тестирования ErrorBoundary
export const BugButton = () => {
	const [error, setError] = useState(false);
	const onThrow = () => { setError(true); };

	useEffect(() => {
		if (error) {
			throw new Error();
		}
	}, [error]);

	return (
		// eslint-disable-next-line i18next/no-literal-string
		<button onClick={onThrow}> Create Error </button>
	);
};
