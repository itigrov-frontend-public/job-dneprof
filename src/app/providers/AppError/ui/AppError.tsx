import { Component, type ErrorInfo, type ReactNode, Suspense } from 'react';
import { ContentError } from 'features/ContentError';
import { ContentErrorCode } from 'features/ContentError/type/ContentError';
import Loader from 'shared/ui/Loader';

interface AppErrorProps {
	children: ReactNode;
}

interface AppErrorState {
	hasError: boolean;
}

class AppError
	extends Component<AppErrorProps, AppErrorState> {
	constructor (props: AppErrorProps) {
		super(props);
		this.state = { hasError: false };
	}

	// eslint-disable-next-line n/handle-callback-err
	static getDerivedStateFromError (error: Error) {
		return { hasError: true };
	}

	componentDidCatch (error: Error, errorInfo: ErrorInfo) {
		console.log(error, errorInfo);
	}

	render () {
		const { hasError } = this.state;
		const { children } = this.props;

		if (hasError) {
			return (
				<Suspense fallback={<Loader/>}>
					<main className="main">
						<ContentError code={ContentErrorCode.error500}/>
					</main>
				</Suspense>
			);
		}

		return children;
	}
}

export default AppError;
