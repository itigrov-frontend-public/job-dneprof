import { type FC, lazy } from 'react';
import { type RouteProps } from 'react-router-dom';
import { AppRoutePath, AppRouteType } from 'shared/config/types/AppRouteType';

const MainPageLazy = lazy<FC>(async () => await import('pages/MainPage'));
const CatalogGroupPageLazy = lazy<FC>(async () => await import('pages/Catalog'));
const CatalogCardPageLazy = lazy<FC>(async () => await import('pages/CatalogCard'));
const ContentPageLazy = lazy(async () => await import('pages/ContentPage'));
const FavouritePageLazy = lazy<FC>(async () => await import('pages/FavouritePage'));
const ErrorPageLazy = lazy<FC>(async () => await import('pages/ErrorPage'));

/*
const MainPageLazy = lazy<FC>(async () => await new Promise((resolve) => {
	setTimeout(() => { resolve(import('pages/MainPage')); }, 1500);
}));

const CatalogGroupPageLazy = lazy<FC>(async () => await new Promise((resolve) => {
	setTimeout(() => { resolve(import('pages/Catalog')); }, 1500);
}));

const CatalogCardPageLazy = lazy<FC>(async () => await new Promise((resolve) => {
	setTimeout(() => { resolve(import('pages/CatalogCard')); }, 1500);
}));

const ContentPageLazy = lazy<FC>(async () => await new Promise((resolve) => {
	setTimeout(() => { resolve(import('pages/ContentPage')); }, 1500);
}));

const FavouritePageLazy = lazy<FC>(async () => await new Promise((resolve) => {
	setTimeout(() => { resolve(import('pages/FavouritePage')); }, 1500);
}));

const ErrorPageLazy = lazy<FC>(async () => await new Promise((resolve) => {
	setTimeout(() => { resolve(import('pages/ErrorPage')); }, 1500);
}));
*/

// Подключение нужного модуля [pages]
export const AppRoutePages: Record<AppRouteType, RouteProps> = {
	[AppRouteType.MainPage]: {
		path: AppRoutePath.MainPage,
		element: <MainPageLazy />
	},

	[AppRouteType.CatalogGroupPage]: {
		path: AppRoutePath.CatalogGroupPage,
		element: <CatalogGroupPageLazy />
	},
	[AppRouteType.CatalogGroupRoot]: {
		path: AppRoutePath.CatalogGroupRoot,
		element: <CatalogGroupPageLazy />
	},
	[AppRouteType.CatalogCardPage]: {
		path: AppRoutePath.CatalogCard,
		element: <CatalogCardPageLazy />
	},

	[AppRouteType.ContentPage]: {
		path: AppRoutePath.ContentPage,
		element: <ContentPageLazy />
	},

	[AppRouteType.FavouritePage]: {
		path: AppRoutePath.FavouritePage,
		element: <FavouritePageLazy />
	},

	[AppRouteType.ErrorPage]: {
		path: AppRoutePath.ErrorPage,
		element: <ErrorPageLazy />
	}

};
