import { memo, Suspense } from 'react';
import { Route, Routes } from 'react-router-dom';
import Loader from 'shared/ui/Loader';

import { AppRoutePages } from '../config/AppRoutePages';

const AppRouter = () => {
	return (
		<Suspense fallback={<Loader/>}>
			<Routes>
				{
					Object.values(AppRoutePages).map(
						({ element, path }) => (
							<Route
								key={path}
								path={path}
								element={element}
							/>
						)
					)
				}
			</Routes>
		</Suspense>
	);
};

export default memo(AppRouter);
