import { type FC } from 'react'
import AppRouter from 'app/providers/AppRouter'
import { Footer } from 'widget/Footer'
import { Header } from 'widget/Header'

import 'shared/config/styles/index.scss';

const AppTemplate: FC = () => {
	return (
		<>
			<Header />
			<main className="main mainColumns-">
				<AppRouter />
			</main>
			<Footer />
		</>
	)
}

export default AppTemplate
