import { type Meta, type StoryObj } from '@storybook/react';
import { themeNames } from 'shared/config/types/Theme';
import { ThemeDecorator } from 'shared/devTests/storybook';

const addStorybookTheme = (meta: Meta, Props = {}, themeInt = 1) => {
	type Story = StoryObj<typeof meta>;

	let theme = themeNames.LIGHT;
	switch (themeInt) {
		case 1: theme = themeNames.LIGHT; break;
		case 2: theme = themeNames.DARK; break;
	}
	const Primary: Story = Props;
	Primary.decorators = [(Story) => ThemeDecorator(Story)(theme)];

	return Primary
}

export default addStorybookTheme
