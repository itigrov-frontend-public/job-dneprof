import { type FC, memo } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'

import ImgLogo from './logo.svg'

import cn from './style.module.scss'

const Logo: FC = () => {
	const { t } = useTranslation()

	// __IS_DEV__ && console.log('RENDER: Logo');

	return (
		<Link to='/' title={t('text-MainPage')}>
			<ImgLogo
				className={cn.logoThema}
				width={165}
				height={42}
			/>
		</Link>
	)
}

export default memo(Logo)
