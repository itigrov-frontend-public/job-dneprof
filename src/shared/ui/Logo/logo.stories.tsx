import type { Meta, StoryObj } from '@storybook/react';
import { themeNames } from 'shared/config/types/Theme';
import { ThemeDecoratorStory } from 'shared/devTests/storybook';

import Logo from './index';

const meta = {
	title: 'shared/Logo',
	component: Logo,
	// This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/react/writing-docs/autodocs
	// tags: ['autodocs'],
	argTypes: {
		// backgroundColor: { control: 'color' },
	},
	parameters: {
		layout: 'centered' // centered | fullscreen | undefined
	}
} satisfies Meta<typeof Logo>;

export default meta;

type Story = StoryObj<typeof meta>;

export const LogoAll: Story = {};
LogoAll.decorators = [(Story, context) => ThemeDecoratorStory(Story, context)];

export const LogoRuLight: Story = {};
LogoRuLight.decorators = [(Story, context) => ThemeDecoratorStory(Story, context, themeNames.LIGHT)];

export const LogoRuDark: Story = {};
LogoRuDark.decorators = [(Story, context) => ThemeDecoratorStory(Story, context, themeNames.DARK)];

//
//
//

// const testMeta = meta;
// testMeta.title = 'test/' + testMeta.title;
// type TestStory = StoryObj<typeof testMeta>;

// console.log(testMeta);

// export const TestLogoRuLight: TestStory = {};
// TestLogoRuLight.decorators = [(Story, context) => ThemeDecoratorStory(Story, context, themeNames.LIGHT)];

// export const TestLogoRuDark: TestStory = {};
// TestLogoRuDark.decorators = [(Story, context) => ThemeDecoratorStory(Story, context, themeNames.DARK)];

// ! Variant 1
// type Story = StoryObj<typeof meta>;
// export const Primary: Story = {};

// ! Variant 2
// type Story = StoryObj<typeof meta>;

// export const Primary: Story = {};
// Primary.decorators = [(Story) => ThemeDecorator(Story)(themeNames.LIGHT)];

// export const PrimaryDark: Story = {};
// PrimaryDark.decorators = [(Story) => ThemeDecorator(Story)(themeNames.DARK)];

// ! Variant 3
// export const Primary = addStorybookTheme(meta, {}, 1);
// export const PrimaryDark = addStorybookTheme(meta, {}, 2);
