import { type AnchorHTMLAttributes, type FC, memo } from 'react'
import { classNames } from 'shared/helpers/classNames'
import { Icon } from 'shared/ui/Icon'

import cn from '../styles/buttonHamburger.module.scss'

// interface IButtonHamburger extends ButtonHTMLAttributes<HTMLButtonElement> {
interface IButtonHamburger extends AnchorHTMLAttributes<HTMLAnchorElement> {
	open?: boolean
}

const ButtonHamburger: FC<IButtonHamburger> = (props) => {
	const {
		open = false,
		...otherProps
	} = props

	return (
		<Icon hover className={classNames(cn.buttonHamburger, { [cn.buttonHamburger_open]: open })} {...otherProps}>
			<div className={classNames(cn.buttonHamburger, { }, [cn.buttonHamburger_in])}>
				<div className={classNames(cn.left)}></div>
				<div className={classNames(cn.right)}></div>
			</div>
		</Icon>
	)
}

export default memo(ButtonHamburger)
