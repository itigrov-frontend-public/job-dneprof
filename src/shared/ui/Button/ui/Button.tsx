import { type FC, memo } from 'react'
import { classNames } from 'shared/helpers/classNames'

import { type ButtonProps, ButtonSizeW, ButtonTheme } from '../types/buttonType'

import cn from '../styles/buttom.module.scss'

const Button: FC<ButtonProps> = (props) => {
	const {
		theme = ButtonTheme.main,
		sizeW = ButtonSizeW.auto,
		children,
		...otherProps
	} = props

	return (
		<button
			className={classNames(cn.button, {}, [cn[theme], cn[sizeW]])}
			{...otherProps}
		>
			{children}
		</button>
	)
}

export default memo(Button)
