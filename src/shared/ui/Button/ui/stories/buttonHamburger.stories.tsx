import type { Meta, StoryObj } from '@storybook/react';
import { themeNames } from 'shared/config/types/Theme';
import { ThemeDecoratorStory } from 'shared/devTests/storybook';

import ButtonHamburger from '../ButtonHamburger';

const meta = {
	title: 'shared/Button/ButtonHamburger',
	component: ButtonHamburger,
	args: {},
	parameters: {
		layout: 'centered' // centered | fullscreen | undefined
	}
} satisfies Meta<typeof ButtonHamburger>;

export default meta;

type Story = StoryObj<typeof meta>;

export const ButtonHamburgerAll: Story = {};
ButtonHamburgerAll.decorators = [(Story, context) => ThemeDecoratorStory(Story, context)];

export const ButtonHamburgerOpenDark: Story = { args: { open: true } };
ButtonHamburgerOpenDark.decorators = [(Story, context) => ThemeDecoratorStory(Story, context, themeNames.DARK)];
