import type { Meta, StoryObj } from '@storybook/react';
import { themeNames } from 'shared/config/types/Theme';
import { ThemeDecoratorStory } from 'shared/devTests/storybook';
import IconSvg from 'shared/static/svg/icons/forStorybook.svg';

import { ButtonSizeW, ButtonTheme } from '../../types/buttonType';
import ButtonIcon from '../ButtonIcon';

const meta = {
	title: 'shared/Button/ButtonIcon',
	component: ButtonIcon,
	args: {
		children: 'Текст на кнопке'
	},
	parameters: {
		layout: 'centered' // centered | fullscreen | undefined
	}
} satisfies Meta<typeof ButtonIcon>;

export default meta;

type Story = StoryObj<typeof meta>;

export const ButtonIconAll: Story = {
	args: {
		theme: ButtonTheme.main,
		sizeW: ButtonSizeW.auto,
		icon: <IconSvg/>
	}
};
ButtonIconAll.decorators = [(Story, context) => ThemeDecoratorStory(Story, context)];

export const ButtonAllNoIcon: Story = { args: { icon: undefined } };
ButtonAllNoIcon.decorators = [(Story, context) => ThemeDecoratorStory(Story, context)];

export const ButtonAllMainsecond: Story = { args: { theme: ButtonTheme.mainSecond, sizeW: ButtonSizeW.m } };
ButtonAllMainsecond.decorators = [(Story, context) => ThemeDecoratorStory(Story, context)];

export const ButtonIconDark: Story = { args: { icon: <IconSvg/> } };
ButtonIconDark.decorators = [(Story, context) => ThemeDecoratorStory(Story, context, themeNames.DARK)];
