import type { Meta, StoryObj } from '@storybook/react';
import { themeNames } from 'shared/config/types/Theme';
import { ThemeDecoratorStory } from 'shared/devTests/storybook';

import { ButtonSizeW, ButtonTheme } from '../../types/buttonType';
import Button from '../Button';

const meta = {
	title: 'shared/Button/Button',
	component: Button,
	args: {
		children: 'Текст на кнопке'
	},
	parameters: {
		layout: 'centered' // centered | fullscreen | undefined
	}
} satisfies Meta<typeof Button>;

export default meta;

type Story = StoryObj<typeof meta>;

export const ButtonAll: Story = {
	args: {
		theme: ButtonTheme.main,
		sizeW: ButtonSizeW.auto
	}
};
ButtonAll.decorators = [(Story, context) => ThemeDecoratorStory(Story, context)];

export const ButtonAllNoneM: Story = { args: { theme: ButtonTheme.none, sizeW: ButtonSizeW.m } };
ButtonAllNoneM.decorators = [(Story, context) => ThemeDecoratorStory(Story, context)];

export const ButtonAllMainsecondS: Story = { args: { theme: ButtonTheme.mainSecond, sizeW: ButtonSizeW.s } };
ButtonAllMainsecondS.decorators = [(Story, context) => ThemeDecoratorStory(Story, context)];

export const ButtonDark: Story = {};
ButtonDark.decorators = [(Story, context) => ThemeDecoratorStory(Story, context, themeNames.DARK)];
