import { type FC, memo } from 'react'
import { classNames } from 'shared/helpers/classNames'

import { type ButtonIconProps, ButtonSizeW, ButtonTheme } from '../types/buttonType'

import cn from '../styles/buttom.module.scss'

const Button: FC<ButtonIconProps> = (props) => {
	const {
		theme = ButtonTheme.main,
		sizeW = ButtonSizeW.auto,
		className = false,
		icon,
		children,
		...otherProps
	} = props

	return (
		<button
			className={classNames(cn.button, { className }, [cn[theme], cn[sizeW]])}
			{...otherProps}
		>
			<div className={cn.buttonIn}>
				{icon && <span className={cn.buttonIcon}>{icon}</span>}
				{children}
			</div>
		</button>
	)
}

export default memo(Button)
