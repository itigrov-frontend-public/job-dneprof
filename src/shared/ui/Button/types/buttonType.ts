import { type ButtonHTMLAttributes, type PropsWithChildren, type ReactNode } from 'react'

export enum ButtonTheme {
	main = 'bgMain',
	mainSecond = 'bgMainSecond',
	none = 'bgNone',
	// bgNoneLight = 'bgNoneLight',
	// bgNoneDark = 'bgNoneDark',
}

export enum ButtonSizeW {
	max = 'withMax',
	auto = 'withAuto',
	s = 'withS',
	m = 'withM',
	noText = 'noText',
}

export interface ButtonProps extends PropsWithChildren, ButtonHTMLAttributes<HTMLButtonElement> {
	theme?: ButtonTheme
	// icon?: ReactNode
	sizeW?: ButtonSizeW
}

export interface ButtonIconProps extends PropsWithChildren, ButtonHTMLAttributes<HTMLButtonElement> {
	theme?: ButtonTheme
	sizeW?: ButtonSizeW
	icon?: ReactNode
}
