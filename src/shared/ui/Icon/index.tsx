/* eslint-disable no-tabs */
import Icon from './ui/Icon';
import IconLink from './ui/IconLink';
import IconText from './ui/IconText';
import IconTextLink from './ui/IconTextLink';
export { Icon, IconLink, IconText, IconTextLink }

// icon 					[tag hover alert className ... 						children]
// iconText 			[tag hover alert className ... icon text 	children]
// iconLink 								[alert className ... 						children]
// iconTextLink 						[alert className ... icon text 	children]
