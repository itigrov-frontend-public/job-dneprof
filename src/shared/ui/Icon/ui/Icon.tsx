import { createElement, type FC, type HTMLAttributes, memo, type PropsWithChildren, type ReactNode } from 'react'
import { classNames } from 'shared/helpers/classNames'

import cn from '../styles/icon.module.scss'

// interface IconProps extends PropsWithChildren, ButtonHTMLAttributes<HTMLButtonElement> {
interface IconProps extends PropsWithChildren, HTMLAttributes<HTMLElement> {
	tag?: string
	hover?: boolean
	alert?: string | number | undefined | false
	icon?: ReactNode
}

const Icon: FC<IconProps> = (props: IconProps) => {
	const {
		tag = 'button',
		hover = false,
		className = '',
		alert,
		icon,
		children,
		...otherProps
	} = props

	const alertContent = alert ? <span className={cn.alert}>{alert}</span> : <></>;

	return createElement(tag,
		{
			className: classNames(cn.icon, { [cn.iconHover]: hover }, [className]),
			...otherProps
		},
		alertContent,
		icon,
		children
	);
};
export default memo(Icon)
