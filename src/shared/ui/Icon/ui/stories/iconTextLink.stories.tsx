import type { Meta, StoryObj } from '@storybook/react';
import { themeNames } from 'shared/config/types/Theme';
import { ThemeDecoratorStory } from 'shared/devTests/storybook';
import IconSvg from 'shared/static/svg/icons/forStorybook.svg';

import IconTextLink from '../IconTextLink';

const meta = {
	title: 'shared/Icon/IconTextLink',
	component: IconTextLink,
	argTypes: {},
	args: {
		to: '/',
		icon: <IconSvg/>,
		text: 'Тестовый текст'
	},
	parameters: {
		layout: 'centered' // centered | fullscreen | undefined
	}
} satisfies Meta<typeof IconTextLink>;

export default meta;

type Story = StoryObj<typeof meta>;

export const IconTextLinkAll: Story = {
	args: {
		alert: 'W'
	}
};
IconTextLinkAll.decorators = [(Story, context) => ThemeDecoratorStory(Story, context)];

export const IconTextLinkDark: Story = { args: { alert: 77 } };
IconTextLinkDark.decorators = [(Story, context) => ThemeDecoratorStory(Story, context, themeNames.DARK)];
