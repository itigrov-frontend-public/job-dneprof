import type { Meta, StoryObj } from '@storybook/react';
import { themeNames } from 'shared/config/types/Theme';
import { ThemeDecoratorStory } from 'shared/devTests/storybook';
import IconSvg from 'shared/static/svg/icons/forStorybook.svg';

import Icon from '../Icon';

const meta = {
	title: 'shared/Icon/Icon',
	component: Icon,
	argTypes: {},
	args: {
		children: <IconSvg/>
	},
	parameters: {
		layout: 'centered' // centered | fullscreen | undefined
	}
} satisfies Meta<typeof Icon>;

export default meta;

type Story = StoryObj<typeof meta>;

export const IconAll: Story = {
	args: {
		hover: true,
		alert: 'W'
	}
};
IconAll.decorators = [(Story, context) => ThemeDecoratorStory(Story, context)];

export const IconDark: Story = { args: { alert: 77 } };
IconDark.decorators = [(Story, context) => ThemeDecoratorStory(Story, context, themeNames.DARK)];
