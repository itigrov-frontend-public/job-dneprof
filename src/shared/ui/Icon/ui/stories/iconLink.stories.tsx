import type { Meta, StoryObj } from '@storybook/react';
import { themeNames } from 'shared/config/types/Theme';
import { ThemeDecoratorStory } from 'shared/devTests/storybook';
import IconSvg from 'shared/static/svg/icons/forStorybook.svg';

import IconLink from '../IconLink';

const meta = {
	title: 'shared/Icon/IconLink',
	component: IconLink,
	argTypes: {},
	args: {
		to: '/',
		children: <IconSvg/>
	},
	parameters: {
		layout: 'centered' // centered | fullscreen | undefined
	}
} satisfies Meta<typeof IconLink>;

export default meta;

type Story = StoryObj<typeof meta>;

export const IconLinkAll: Story = {
	args: {

		alert: 'W'
	}
};
IconLinkAll.decorators = [(Story, context) => ThemeDecoratorStory(Story, context)];

export const IconLinkDark: Story = { args: { alert: 77 } };
IconLinkDark.decorators = [(Story, context) => ThemeDecoratorStory(Story, context, themeNames.DARK)];
