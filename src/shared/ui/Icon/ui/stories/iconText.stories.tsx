import type { Meta, StoryObj } from '@storybook/react';
import { themeNames } from 'shared/config/types/Theme';
import { ThemeDecoratorStory } from 'shared/devTests/storybook';
import IconSvg from 'shared/static/svg/icons/forStorybook.svg';

import IconText from '../IconText';

const meta = {
	title: 'shared/Icon/IconText',
	component: IconText,
	argTypes: {},
	args: {
		icon: <IconSvg/>,
		text: 'Тестовый текст'
	},
	parameters: {
		layout: 'centered' // centered | fullscreen | undefined
	}
} satisfies Meta<typeof IconText>;

export default meta;

type Story = StoryObj<typeof meta>;

export const IconTextAll: Story = {
	args: {
		hover: true,
		alert: 'W'
	}
};
IconTextAll.decorators = [(Story, context) => ThemeDecoratorStory(Story, context)];

export const IconTextDark: Story = { args: { alert: 77, hover: true } };
IconTextDark.decorators = [(Story, context) => ThemeDecoratorStory(Story, context, themeNames.DARK)];
