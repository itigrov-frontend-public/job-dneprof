import { type FC, memo, type PropsWithChildren, type ReactNode } from 'react'
import { Link, type LinkProps } from 'react-router-dom'
import { classNames } from 'shared/helpers/classNames'

import cn from '../styles/icon.module.scss'

interface IconProps extends PropsWithChildren, LinkProps {
	alert?: string | number | undefined | false
	icon?: ReactNode
}

const IconLink: FC<IconProps> = (props) => {
	const {
		className = '',
		alert,
		icon,
		children,
		...otherProps
	} = props

	const alertContent = alert ? <span className={cn.alert}>{alert}</span> : <></>;

	return (
		<Link className={classNames(cn.icon, { [cn.iconHover]: true }, [className])} {...otherProps}>
			{alertContent}
			{icon}
			{children}
		</Link>
	)
};
export default memo(IconLink)
