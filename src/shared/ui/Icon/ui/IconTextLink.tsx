import { type FC, memo, type PropsWithChildren, type ReactNode } from 'react'
import { type LinkProps } from 'react-router-dom'
import { Link } from 'react-router-dom'
import { classNames } from 'shared/helpers/classNames'

import cn from '../styles/icon.module.scss'

interface IconProps extends PropsWithChildren, LinkProps {
	alert?: string | number | undefined | false

	icon?: ReactNode
	text?: ReactNode
}

const IconTextLink: FC<IconProps> = (props: IconProps) => {
	const {
		className = '',
		alert,

		icon,
		text,

		children,
		...otherProps
	} = props

	const alertContent = alert ? <span className={cn.alert}>{alert}</span> : <></>;
	const iconContent = icon
		? <span className={classNames(cn.icon, { }, [className])} >{alertContent}{icon}</span>
		: <></>;
	const textContent = text ? <span className={cn.text}>{text}</span> : <></>;

	return (
		<Link className={classNames(cn.iconText, { [cn.iconTextHover]: true }, [className])} {...otherProps}>
			{iconContent}
			{textContent}
			{children}
		</Link>
	)
};
export default memo(IconTextLink)
