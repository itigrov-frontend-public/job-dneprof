import { type AnchorHTMLAttributes, createElement, type FC, memo, type PropsWithChildren, type ReactNode } from 'react'
import { classNames } from 'shared/helpers/classNames'

import cn from '../styles/icon.module.scss'

interface IconProps extends PropsWithChildren, AnchorHTMLAttributes<HTMLAnchorElement> {
// interface IconProps extends PropsWithChildren, ButtonHTMLAttributes<HTMLButtonElement> {
	tag?: string
	hover?: boolean
	alert?: string | number | undefined | false

	icon?: ReactNode
	text?: ReactNode
}

const IconText: FC<IconProps> = (props: IconProps) => {
	const {
		tag = 'button',
		hover = false,
		className = '',
		alert,

		icon,
		text,

		children,
		...otherProps
	} = props

	const alertContent = alert ? <span className={cn.alert}>{alert}</span> : <></>;
	const iconContent = icon
		? <span className={classNames(cn.icon, {}, [className])} >{alertContent}{icon}</span>
		: <></>;
	const textContent = text ? <span className={cn.text}>{text}</span> : <></>;

	return createElement(tag,
		{
			className: classNames(cn.iconText, { [cn.iconTextHover]: hover }, [className]),
			...otherProps
		},
		iconContent,
		textContent,
		children
	);
};
export default memo(IconText)
