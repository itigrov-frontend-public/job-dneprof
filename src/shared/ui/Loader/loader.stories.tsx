import type { Meta, StoryObj } from '@storybook/react';
import { i18nNames } from 'shared/config/types/i18n';
import { themeNames } from 'shared/config/types/Theme';
import { I18nDecorator, ThemeDecoratorStory } from 'shared/devTests/storybook';

import Loader from './index';

const meta = {
	title: 'shared/Loader',
	component: Loader,
	argTypes: {},
	parameters: {
		layout: 'centered' // centered | fullscreen | undefined
	}
} satisfies Meta<typeof Loader>;

export default meta;

type Story = StoryObj<typeof meta>;

export const LoaderAll: Story = {};
LoaderAll.decorators = [
	(Story, context) => ThemeDecoratorStory(Story, context),
	(Story, context) => I18nDecorator(Story, context)
];

export const LoaderLight: Story = {};
LoaderLight.decorators = [
	(Story, context) => ThemeDecoratorStory(Story, context, themeNames.LIGHT),
	(Story, context) => I18nDecorator(Story, context, i18nNames.ru)
];

export const LoaderDark: Story = {};
LoaderDark.decorators = [
	(Story, context) => ThemeDecoratorStory(Story, context, themeNames.DARK),
	(Story, context) => I18nDecorator(Story, context, i18nNames.ru)
];
