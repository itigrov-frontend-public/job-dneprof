import { type FC, memo } from 'react'

import cn from './loader.module.scss'

const Loader: FC = () => {
	return (
		<div className={cn.ldsEllipsisBefore}><div className={cn.ldsEllipsis}><div></div><div></div><div></div><div></div></div></div>
	)
}

export default memo(Loader)
