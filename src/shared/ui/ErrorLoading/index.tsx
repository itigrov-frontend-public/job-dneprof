import { type FC, memo } from 'react';
import { useTranslation } from 'react-i18next'
import SvgLoadingError from 'shared/static/svg/icons/loadingError.svg'

import cn from './styles/errorText.module.scss'

const ErrorLoading: FC = () => {
	const { t } = useTranslation('error');

	return (
		<div className={cn.errorText + ' pBotBox'}>
			<div className={cn.icon}><SvgLoadingError/></div>
			{t('ErrorText-loading')}
		</div>
	)
}

export default memo(ErrorLoading)
