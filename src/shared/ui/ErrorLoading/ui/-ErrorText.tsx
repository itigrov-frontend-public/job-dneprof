
import { type FC, type PropsWithChildren } from 'react'
import { useTranslation } from 'react-i18next'
import SvgLoadingError from 'shared/static/svg/icons/loadingError.svg'

import cn from './styles/errorText.module.scss'

export enum IErrorTextIcon {
	none = 0,
	loading = 1
}

export enum IErrorTextText {
	none = 0,
	loading = 1
}

interface IErrorText extends PropsWithChildren {
	icon?: IErrorTextIcon
	text?: IErrorTextText
}

const ErrorText: FC<IErrorText> = ({ icon = IErrorTextIcon.loading, text = IErrorTextText.loading, children }) => {
	const { t } = useTranslation('error');
	console.log('RENDER: ErrorText');

	let svgIcon;
	switch (icon) {
		case IErrorTextIcon.loading:
			svgIcon = <SvgLoadingError/>
			break;
	}

	let fcText;
	switch (text) {
		case IErrorTextText.loading:
			fcText = t('ErrorText-loading')
			break;
	}

	return (
		<div className={cn.errorText}>
			{svgIcon && <div className={cn.icon}>{svgIcon}</div>}{fcText}{children}
		</div>
	)
}

export default ErrorText
