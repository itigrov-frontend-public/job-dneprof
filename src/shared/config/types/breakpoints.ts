export enum breakpoints {
	mobile = 320,
	tablet = 640,
	pc = 960,
}

export interface BreakpointsNames {
	isMobile: boolean
	isTabletLess: boolean
	isTablet: boolean
	isTabletMore: boolean
	isPc: boolean
}

export interface IBreakpointsSlice {
	isMobile: boolean
	isTabletLess: boolean
	isTablet: boolean
	isTabletMore: boolean
	isPc: boolean
}
