export interface IImage {
	id: number
	image: string | null
	name: string | null
}
