import { type IImage } from './appTypes'

export const CATALOG_CARDS_LIMIT_DEFAULT = 30

export interface ICatalogGroup {
	id: number,
	huu: string,
	name: string,
	// image: IImage[]
}

export interface ICatalogGroupPage {
	id: number,
	huu: string,
	name: string,
	name2: string,
	// text: string | null,
	// image: string | null,
}

interface ICatalogCard_group {
	huu: string
}

export interface ICatalogCard {
	id: number,
	// groupId: number,
	huu: string,
	name: string,
	notice: string,
	price: number,
	priceOld: number,
	priceType: number,
	// text: string | null,
	image: IImage[] | null,
	group: ICatalogCard_group
}
export interface ICatalogCardPage extends ICatalogCard {
	avito: string,
	text: string,
	images: IImage[]
}
