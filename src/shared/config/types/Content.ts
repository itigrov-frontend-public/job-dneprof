export interface IContentCard {
	id: number,
	huu: string,
	name: string,
	text: string | null,
}
