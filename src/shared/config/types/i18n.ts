export enum i18nNames {
	ru = 'ru',
	en = 'en',
}

export const i18nNameNames: Record<i18nNames, string> = {
	[i18nNames.ru]: 'Русский',
	[i18nNames.en]: 'English'
};

export const i18nLocaleNames: Record<i18nNames, string> = {
	[i18nNames.ru]: 'ru-RU',
	[i18nNames.en]: 'en-US'
};
