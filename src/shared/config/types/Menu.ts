export interface IMenuCard {
	id: number,
	name: string,
	url: string,
}

export interface IMenuSlice {
	open: boolean | undefined
}
