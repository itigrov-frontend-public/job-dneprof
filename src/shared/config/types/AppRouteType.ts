// Все возможные [pages]
export enum AppRouteType {
	MainPage = 'MainPage',
	ContentPage = 'ContentPage',
	FavouritePage = 'FavouritePage',

	CatalogGroupRoot = 'CatalogGroupRoot',
	CatalogGroupPage = 'CatalogGroupPage',
	CatalogCardPage = 'CatalogCard',

	ErrorPage = 'ErrorPage',
}

// URL ко всем [pages]
export const AppRoutePath: Record<AppRouteType, string> = {
	[AppRouteType.MainPage]: '/',

	[AppRouteType.FavouritePage]: '/favourite',

	[AppRouteType.CatalogGroupRoot]: '/catalog',
	[AppRouteType.CatalogGroupPage]: '/catalog/:huu',
	[AppRouteType.CatalogCardPage]: '/catalog/:groupHuu/:huu',

	[AppRouteType.ContentPage]: '/:huu',

	// last
	[AppRouteType.ErrorPage]: '*'
};
