export enum themeNames {
	LIGHT = 'light',
	DARK = 'dark',
}

export interface IThemeSlice {
	theme: themeNames
}

export const themeMainColors: Record<themeNames, string> = {
	[themeNames.LIGHT]: '#02CC9B',
	[themeNames.DARK]: '#209cee'
}

export const themeBgColors: Record<themeNames, string> = {
	[themeNames.LIGHT]: '#DFDFDF',
	[themeNames.DARK]: '#1C1C1C'
}
