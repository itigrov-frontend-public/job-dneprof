export const apiBaseUrl = 'https://api.dneprof.ru/'
// export const apiBaseUrl = 'http://localhost:5001'
export const apiMenuCards = '/api/menu'
export const apiContentCard = '/api/content'

export const apiCatalogGroup = '/api/catalog/group'
export const apiCatalogGroups = '/api/catalog/groups'
export const apiCatalogCard = '/api/catalog/card'
export const apiCatalogCards = '/api/catalog/cards'
