import type { AnyAction, CombinedState, Reducer, ReducersMapObject } from '@reduxjs/toolkit';
import { type ToolkitStore } from '@reduxjs/toolkit/dist/configureStore';
import { type appApiReducerType } from 'shared/components/appApi';
import { type IappParamSlice } from 'shared/components/appParam';
import { type IBreakpointsSlice } from 'shared/config/types/breakpoints';
import { type IFavouriteSlice } from 'shared/config/types/Favourite';
import { type IMenuSlice } from 'shared/config/types/Menu';
import { type IThemeSlice } from 'shared/config/types/Theme';

export interface StateSchema {
	theme: IThemeSlice;
	breakpoints: IBreakpointsSlice;
	appApi: appApiReducerType; // for async RTKQuery
	appParam: IappParamSlice;
	favourite: IFavouriteSlice;

	// Асинхронные редюсеры
	menu?: IMenuSlice;
}

// BEGIN: reducerManager
export type StateSchemaKey = keyof StateSchema;

export interface ReducerManager {
	getReducerMap: () => ReducersMapObject<StateSchema>;
	reduce: (state: StateSchema, action: AnyAction) => CombinedState<StateSchema>;
	add: (key: StateSchemaKey, reducer: Reducer) => void;
	remove: (key: StateSchemaKey) => void;
}

// export type ReducerManager = typeof reducerManager; // in AppStore/index
export interface IStore extends ToolkitStore {
	reducerManager: ReducerManager;
}
// END: reducerManager
