import { useEffect } from 'react';
import { useDispatch, useStore } from 'react-redux';
import { type Reducer } from '@reduxjs/toolkit';
import { type IStore, type StateSchemaKey } from 'shared/config/store/AppStore';

const useAddStore = (name: StateSchemaKey, reducer: Reducer) => {
	const store = useStore() as IStore

	const dispatch = useDispatch()

	useEffect(() => {
		store.reducerManager.add(name, reducer);
		dispatch({ type: `@init ${name}` })
		return () => {
			store.reducerManager.remove(name);
			dispatch({ type: `@remove ${name}` })
		}
	}, [])
}

export default useAddStore
