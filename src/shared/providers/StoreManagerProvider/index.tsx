import { type FC, type PropsWithChildren, useEffect } from 'react';
import { useDispatch, useStore } from 'react-redux';
import { type Reducer } from '@reduxjs/toolkit';
import { type IStore, type StateSchemaKey } from 'shared/config/store/AppStore';

export type ReducersList = {
	[name in StateSchemaKey]?: Reducer;
}

interface StoreManagerProviderProps extends PropsWithChildren {
	reducers: ReducersList;
	removeAfterUnmount?: boolean;
}

const StoreManagerProvider: FC<StoreManagerProviderProps> = ({
	children,
	reducers = [],
	removeAfterUnmount = true
}) => {
	const store = useStore() as IStore
	const dispatch = useDispatch();

	useEffect(() => {
		Object.entries(reducers).forEach(([name, reducer]) => {
			store.reducerManager.add(name as StateSchemaKey, reducer);
			dispatch({ type: `@INIT reducer: ${name}` });
		});

		return () => {
			if (removeAfterUnmount) {
				Object.entries(reducers).forEach(([name, reducer]) => {
					store.reducerManager.remove(name as StateSchemaKey);
					dispatch({ type: `@DESTROY reducer: ${name}` });
				});
			}
		};
	}, []);

	return (<>{children}</>)
}

export default StoreManagerProvider
