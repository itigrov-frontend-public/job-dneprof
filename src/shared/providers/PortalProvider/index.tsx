import { type ReactNode } from 'react';
import { createPortal } from 'react-dom';

interface PortalProps {
	children: ReactNode;
	element?: HTMLElement;
}

export const PortalProvider = (props: PortalProps) => {
	const {
		children,
		element = document.body
	} = props;

	if (__IS_APP_TYPE__ === 'storybook') { return (<>{children}</>); }
	return createPortal(children, element);
};
