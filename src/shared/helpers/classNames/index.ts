type classNamesProps = (
	cls: string,
	mods?: Record<string, boolean | string>,
	additional?: string[]
) => string;

export const classNames: classNamesProps = function (cls, mods = {}, additional = []) {
	return [
		cls,
		...additional.filter(Boolean),
		...Object.entries(mods)
			.filter(([_, value]) => Boolean(value))
			.map(([className]) => className)
	]
		.join(' ');
}
