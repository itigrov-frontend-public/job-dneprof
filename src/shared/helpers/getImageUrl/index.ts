type imageUrlPgops = (
	url?: string,
	image?: string | null,
	webp?: boolean,
	retina?: number,
) => string;

export const getImageUrl: imageUrlPgops = function (url = '', image, webp = false, retina = 100) {
	let ret = `/image${url}`
	if (retina > 100) { ret += `/${retina.toString()}` }

	if (image) {
		if (webp) {
			const index = image.lastIndexOf('.')
			const imgName = image.slice(0, index)
			ret += `/${imgName}.webp`
		} else {
			ret += `/${image}`
		}
	} else {
		ret += `/noImage.${webp ? 'webp' : 'png'}`
	}
	return ret
}
