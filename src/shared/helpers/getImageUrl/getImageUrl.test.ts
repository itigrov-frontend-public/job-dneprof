import { getImageUrl } from '.';

describe('getImageUrl', () => {
	test('image.png false 100', () => {
		expect(getImageUrl('/catalog', 'image.png', false, 100)).toBe('/image/catalog/image.png');
	});

	test('image.png', () => {
		expect(getImageUrl('/catalog', 'image.png')).toBe('/image/catalog/image.png');
	});
	test('image.png true 100', () => {
		expect(getImageUrl('/catalog', 'image.png', true, 100)).toBe('/image/catalog/image.webp');
	});
	test('image.png true 200', () => {
		expect(getImageUrl('/catalog', 'image.png', true, 200)).toBe('/image/catalog/200/image.webp');
	});
	test('undefined true 100', () => {
		expect(getImageUrl('/catalog', undefined, true, 100)).toBe('/image/catalog/noImage.webp');
	});
	test('undefined true 200', () => {
		expect(getImageUrl('/catalog', undefined, true, 200)).toBe('/image/catalog/200/noImage.webp');
	});
	test('null true 100', () => {
		expect(getImageUrl('/catalog', null, true, 100)).toBe('/image/catalog/noImage.webp');
	});
	test('null true 200', () => {
		expect(getImageUrl('/catalog', null, true, 200)).toBe('/image/catalog/200/noImage.webp');
	});

	test('undefined image.png false 100', () => {
		expect(getImageUrl(undefined, 'image.png', false, 100)).toBe('/image/image.png');
	});
	test('undefined image.png true 200', () => {
		expect(getImageUrl(undefined, 'image.png', true, 200)).toBe('/image/200/image.webp');
	});
});
