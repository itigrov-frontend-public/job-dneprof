import { i18nLocaleNames } from 'shared/config/types/i18n';

type formatPgops = (
	value: number,
) => string;

export const formatPrice: formatPgops = function (value) {
	return Intl.NumberFormat(i18nLocaleNames.ru, {
		style: 'currency',
		currency: 'RUB',
		maximumFractionDigits: 0
	}).format(Number(value));
}
