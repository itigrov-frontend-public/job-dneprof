import { formatPrice } from './formatPrice';

const space = '\u00a0'

describe('formatPrice', () => {
	test('100', () => {
		expect(formatPrice(100)).toBe(`100${space}₽`);
		expect(formatPrice(100).length).toBe(5);
	});
	test('1000', () => {
		expect(formatPrice(1000)).toBe(`1${space}000${space}₽`);
		expect(formatPrice(1000).length).toBe(7);
	});
	test('3750250', () => {
		expect(formatPrice(3750250)).toBe(`3${space}750${space}250${space}₽`);
		expect(formatPrice(3750250).length).toBe(11);
	});
});
