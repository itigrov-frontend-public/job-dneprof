import { type appStorageList } from 'shared/components/appStorage/types/appStorage';

const appLocalStorage = {
	getItem: function ($key: appStorageList) {
		return localStorage.getItem($key);
	},
	setItem: ($key: appStorageList, $value: string) => {
		localStorage.setItem($key, String($value));
	}

}

export default appLocalStorage
