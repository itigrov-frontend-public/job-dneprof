export enum appStorageList {
	menu = 'menu',
	theme = 'theme',
	favourite = 'favourite'
}

// export const LOCAL_STORAGE_THEME_KEY = 'theme';
// export const LOCAL_STORAGE_MENU_KEY = 'menu';
