import { appStorage, appStorageList } from '.';

describe('classNames', () => {
	test(appStorageList.menu + ' no param', () => {
		expect(appStorage.getItem(appStorageList.menu)).toBe(null);
	});
	test(appStorageList.menu + ' setItem getItem', () => {
		// eslint-disable-next-line @typescript-eslint/no-confusing-void-expression
		expect(appStorage.setItem(appStorageList.menu, 'test string'))
		expect(appStorage.getItem(appStorageList.menu)).toBe('test string');
	});

	test(appStorageList.theme + ' no param', () => {
		expect(appStorage.getItem(appStorageList.theme)).toBe(null);
	});
	test(appStorageList.theme + ' setItem getItem', () => {
		// eslint-disable-next-line @typescript-eslint/no-confusing-void-expression
		expect(appStorage.setItem(appStorageList.theme, 'test string'))
		expect(appStorage.getItem(appStorageList.theme)).toBe('test string');
	});
});
