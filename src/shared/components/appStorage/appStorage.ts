import { type appStorageList } from 'shared/components/appStorage/types/appStorage';

import appLocalStorage from './appLocalStorage';

const appStorage = {
	getItem: function ($key: appStorageList) {
		return appLocalStorage.getItem($key);
	},
	setItem: ($key: appStorageList, $value: string) => {
		appLocalStorage.setItem($key, String($value));
		// example: appCookieStorage.setItem($key, String($value));
	}

}

export default appStorage
