import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { apiBaseUrl } from 'shared/config/store/api';

export const appApi = createApi({
	reducerPath: 'appApi',
	baseQuery: fetchBaseQuery({ baseUrl: apiBaseUrl }),
	endpoints: () => ({})
})

export type appApiReducerType = ReturnType<typeof appApi.reducer>;

// endpoints: (builder) => ({
// // eslint-disable-next-line @typescript-eslint/no-invalid-void-type
// getCards: builder.query<IMenuCard[], void>({
// query: () => ({
// url: apiMenuCards
// })
// })

// })

// export const appApiReducer = appApi.reducer;
// export type appApiType = typeof appApi
// export type StoreState = ReturnType<typeof store.getState>;

// https://github.com/reduxjs/redux-toolkit/discussions/2506
