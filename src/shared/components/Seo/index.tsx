import { type FC, memo } from 'react'
import { Helmet } from 'react-helmet-async'
import { useTranslation } from 'react-i18next'
import { useSelector } from 'react-redux'
import { useLocation } from 'react-router-dom'
import { getTheme } from 'entities/Theme'
import { i18nLocaleNames, i18nNames } from 'shared/config/types/i18n'
import { themeMainColors, themeNames } from 'shared/config/types/Theme'

interface ISeo {
	title?: string
	description?: string
	pageUrl?: string
	imageUrl?: string
	pageFullUrl?: string
	imageFullUrl?: string

	color?: string
	locale?: string
	lang?: string
};

const Seo: FC<ISeo> = (props) => {
	const domainUrl = String(window.location.protocol) + '//' + String(window.location.host)
	const location = useLocation()
	const theme = useSelector(getTheme)
	const { t, i18n } = useTranslation();
	const socialImgUrl = '/favicon/social.png'

	const {
		title = t('title-default'),
		description = t('description-default'),
		imageUrl = socialImgUrl,
		pageUrl = String(location.pathname),
		imageFullUrl = domainUrl + imageUrl,
		pageFullUrl = domainUrl + pageUrl,
		color = String(theme === themeNames.LIGHT ? themeMainColors[themeNames.LIGHT] : themeMainColors[themeNames.DARK]),
		locale = String(i18n.language === i18nNames.ru ? i18nLocaleNames[i18nNames.ru] : i18nLocaleNames[i18nNames.en]),
		lang = String(i18n.language === i18nNames.ru ? i18nNames.ru : i18nNames.en)
	} = props

	if (__IS_APP_TYPE__ === 'storybook') { return (<></>); }

	return (
		<>
			<Helmet
				htmlAttributes={{ lang, dark: theme === themeNames.DARK }}
				title={title}
				meta={[
					{ name: 'description', content: description },
					// { name: 'og:type', content: 'webapp' },
					{ name: 'og:title', content: title },
					{ name: 'og:description', content: description },
					{ name: 'og:image', content: imageFullUrl },
					{ name: 'og:url', content: pageFullUrl },
					{ name: 'og:locale', content: locale },

					{ name: 'theme-color', content: color },

					{ name: 'theme-scheme', content: String(themeNames.LIGHT + ' ' + themeNames.DARK) },
					{
						name: 'theme-color',
						content: String(themeMainColors[themeNames.LIGHT]),
						media: '(prefers-color-scheme: ' + String(themeNames.LIGHT) + ')  or (prefers-color-scheme: no-preference)'
					},
					{ name: 'theme-color', content: String(themeMainColors[themeNames.DARK]), media: '(prefers-color-scheme: ' + String(themeNames.DARK) + ')' },
					{ name: 'msapplication-TileColor', content: color },
					{ name: 'msapplication-TileImage', content: socialImgUrl }
				]}
				base={{ href: pageFullUrl }}
			/>
		</>
	)
}

export default memo(Seo)
