import setAppParamMobile from './helpers/setAppParamMobile';
import setAppParamRetina from './helpers/setAppParamRetina';
import setAppParamScrollWidth from './helpers/setAppParamScrollWidth';
import setAppParamWebp from './helpers/setAppParamWebp';
import { getAppParam } from './selectors/getAppParam';
import { appParamSliceReducer } from './store/appParamSlice';
import type { IappParamSlice } from './types/appParam'

export {
	appParamSliceReducer,
	getAppParam,
	type IappParamSlice,
	setAppParamMobile,
	setAppParamRetina,
	setAppParamScrollWidth,
	setAppParamWebp
}
