export interface IappParamSlice {
	isWebp: boolean | undefined
	isMobile: boolean | undefined
	scrollWidth: number | undefined
	isRetina: number | undefined
	// isRetina20: boolean | undefined,
	// isRetina13: boolean | undefined
}
