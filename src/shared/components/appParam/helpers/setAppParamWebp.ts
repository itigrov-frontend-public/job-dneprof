import { useDispatch } from 'react-redux';

import { appParamSliceActions } from '../store/appParamSlice';

const setAppParamWebp = () => {
	const dispatch = useDispatch()

	const webp = new Image();
	webp.src = 'data:image/webp;base64,UklGRkQAAABXRUJQVlA4WAoAAAAQAAAAAQAAAQAAQUxQSAUAAAAAAAAAAABWUDggGAAAADABAJ0BKgIAAgACADQlpAADcAD++5QAAA=='
	webp.onload = webp.onerror = function () {
		dispatch(appParamSliceActions.setAppParamWebp(webp.height === 2));
	}

	// __IS_DEV__ && console.log('---===setAppParamWebp===---');
}
export default setAppParamWebp
