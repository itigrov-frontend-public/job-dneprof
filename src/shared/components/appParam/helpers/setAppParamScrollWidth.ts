import { useDispatch } from 'react-redux';

import { appParamSliceActions } from '../store/appParamSlice';

const setAppParamScrollWidth = () => {
	const dispatch = useDispatch()

	const div = document.createElement('div');
	div.style.overflowY = 'scroll';
	div.style.width = '50px';
	div.style.height = '50px';
	document.body.append(div);
	const scrollWidth = div.offsetWidth - div.clientWidth;
	div.remove();
	dispatch(appParamSliceActions.setAppParamScroll(scrollWidth));

	// __IS_DEV__ && console.log('---===setAppParamScrollWidth===---');
}
export default setAppParamScrollWidth
