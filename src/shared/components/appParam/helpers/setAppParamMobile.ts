import { useDispatch } from 'react-redux';

import { appParamSliceActions } from '../store/appParamSlice';

const setAppParamMobile = () => {
	const dispatch = useDispatch()

	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
		dispatch(appParamSliceActions.setAppParamMobile(true));
	} else {
		dispatch(appParamSliceActions.setAppParamMobile(false));
	}

	// __IS_DEV__ && console.log('---===setAppParamMobile===---');
}
export default setAppParamMobile
