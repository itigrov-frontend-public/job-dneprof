import { useDispatch } from 'react-redux';

import { appParamSliceActions } from '../store/appParamSlice';

const setAppParamRetina = () => {
	const dispatch = useDispatch()

	// __IS_DEV__ && console.log('---===setAppParamRetina===---');

	if ((
		(window.matchMedia &&
			(
				window.matchMedia('only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx), only screen and (min-resolution: 75.6dpcm)').matches ||
				// eslint-disable-next-line max-len
				window.matchMedia('only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (min--moz-device-pixel-ratio: 2), only screen and (min-device-pixel-ratio: 2)').matches
			)
		) || (window.devicePixelRatio && window.devicePixelRatio >= 2)
	) // && /(iPad|iPhone|iPod)/g.test(navigator.userAgent)
	) {
		dispatch(appParamSliceActions.setAppParamRetina(200));
		return
	}

	/*
	// ! Don't delete
	if (
		(window.matchMedia &&
			(
				window.matchMedia('only screen and (min-resolution: 124dpi), only screen and (min-resolution: 1.3dppx), only screen and (min-resolution: 48.8dpcm)').matches ||
				// eslint-disable-next-line max-len
				window.matchMedia('only screen and (-webkit-min-device-pixel-ratio: 1.3), only screen and (-o-min-device-pixel-ratio: 2.6/2), only screen and (min--moz-device-pixel-ratio: 1.3), only screen and (min-device-pixel-ratio: 1.3)').matches
			)
		) || (window.devicePixelRatio && window.devicePixelRatio > 1.3)
	) {
		dispatch(appParamSliceActions.setAppParamRetina(130));
		return
	}
	*/

	dispatch(appParamSliceActions.setAppParamRetina(100));
}
export default setAppParamRetina
