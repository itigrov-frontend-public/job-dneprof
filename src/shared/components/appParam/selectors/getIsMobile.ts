import { type StateSchema } from 'shared/config/store/AppStore';

export const getIsMobile = (state: StateSchema) => state.appParam.isRetina;
