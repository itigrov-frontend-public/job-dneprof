import { type StateSchema } from 'shared/config/store/AppStore';

export const getIsWebp = (state: StateSchema) => state.appParam.isWebp;
