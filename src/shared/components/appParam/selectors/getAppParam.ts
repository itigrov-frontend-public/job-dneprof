import { type StateSchema } from 'shared/config/store/AppStore';

export const getAppParam = (state: StateSchema) => state.appParam;
