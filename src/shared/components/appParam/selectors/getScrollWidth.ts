import { type StateSchema } from 'shared/config/store/AppStore';

export const getScrollWidth = (state: StateSchema) => state.appParam.scrollWidth;
