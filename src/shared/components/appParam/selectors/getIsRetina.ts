import { type StateSchema } from 'shared/config/store/AppStore';

export const getIsRetina = (state: StateSchema) => state.appParam.isRetina;
