import { type DeepPartial } from '@reduxjs/toolkit';

import { type IappParamSlice } from '../types/appParam';

import { appParamSliceActions, appParamSliceReducer } from './appParamSlice';

describe('appParamSlice.test', () => {
	test('setAppParamWebp true', () => {
		const state: DeepPartial<IappParamSlice> = { isWebp: false };
		expect(appParamSliceReducer(
			state as IappParamSlice,
			appParamSliceActions.setAppParamWebp(true)
		)).toEqual({ isWebp: true });
	});

	test('setAppParamMobile true', () => {
		const state: DeepPartial<IappParamSlice> = { isMobile: false };
		expect(appParamSliceReducer(
			state as IappParamSlice,
			appParamSliceActions.setAppParamMobile(true)
		)).toEqual({ isMobile: true });
	});

	test('setAppParamScroll 20', () => {
		const state: DeepPartial<IappParamSlice> = { scrollWidth: undefined };
		expect(appParamSliceReducer(
			state as IappParamSlice,
			appParamSliceActions.setAppParamScroll(20)
		)).toEqual({ scrollWidth: 20 });
	});

	test('setAppParamRetina 100', () => {
		const state: DeepPartial<IappParamSlice> = { isRetina: undefined };
		expect(appParamSliceReducer(
			state as IappParamSlice,
			appParamSliceActions.setAppParamRetina(100)
		)).toEqual({ isRetina: 100 });
	});
});
