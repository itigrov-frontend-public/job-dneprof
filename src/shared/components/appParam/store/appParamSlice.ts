import { createSlice, type PayloadAction } from '@reduxjs/toolkit';
import { type IappParamSlice } from 'shared/components/appParam/types/appParam';

const initialState: IappParamSlice = {
	isWebp: undefined,
	isMobile: undefined,
	scrollWidth: undefined,
	isRetina: undefined
}

export const appParamSlice = createSlice({
	name: 'appParam',
	initialState,
	reducers: {
		setAppParamWebp (state, action: PayloadAction<boolean>) { state.isWebp = action.payload; },
		setAppParamMobile (state, action: PayloadAction<boolean>) { state.isMobile = action.payload; },
		setAppParamScroll (state, action: PayloadAction<number>) { state.scrollWidth = action.payload; },
		setAppParamRetina (state, action: PayloadAction<number>) { state.isRetina = action.payload; }
	}

});

export const appParamSliceReducer = appParamSlice.reducer;
export const appParamSliceActions = appParamSlice.actions;
