module.exports = {
	env: {
		browser: true,
		es2021: true,
		jest: true
	},
	extends: [
		'plugin:react/recommended',
		'standard-with-typescript',
		'plugin:i18next/recommended',
		'plugin:storybook/recommended'
	],
	overrides: [{
		files: ['global.d.ts'],
		rules: {
			'no-undef': 'off'
		}
	}
	],

	parserOptions: {
		parser: '@typescript-eslint/parser',
		project: ['./tsconfig.json'],
		ecmaFeatures: {
			jsx: true
		},
		ecmaVersion: 'latest',
		tsconfigRootDir: __dirname,
		sourceType: 'module'
	},
	plugins: [
		'react',
		'simple-import-sort',
		'@typescript-eslint',
		'i18next',
		'react-hooks'
	],
	rules: {
		'simple-import-sort/exports': 'warn',

		'simple-import-sort/imports': [
			'warn',
			{
				groups: [
					['^react', '^@?\\w'],
					['^(@|components)(/.*|$)'],
					['^\\u0000'],
					['^\\.\\.(?!/?$)', '^\\.\\./?$'],
					['^\\./(?=.*/)(?!/?$)', '^\\.(?!/?$)', '^\\./?$'],
					['^.+\\.?(css)$']
				]
			}
		],

		// TAB вместо пробелов
		indent: ['error', 'tab', { SwitchCase: 1 }],
		'@typescript-eslint/indent': ['error', 'tab'],
		'no-tabs': ['error', {
			allowIndentationTabs: true
		}],

		// FIX: import
		'@typescript-eslint/prefer-nullish-coalescing': 'off',
		'@typescript-eslint/strict-boolean-expressions': 'off',
		// i18next: ---------------
		// 'i18next/no-literal-string': 'off', // Простое отключение i18n
		'i18next/no-literal-string': ['warn', {
			markupOnly: true,
			ignoreAttribute: ['to']
		}],

		'@typescript-eslint/no-misused-promises': [
			'error', { checksVoidReturn: false }
		],

		// OTHER NEED: ------------------------------------
		'max-len': ['error', {
			code: 180,
			ignoreComments: true
		}], // Максимальная длинна строк

		'@typescript-eslint/prefer-includes': 'off', // ! Обязательно

		'react/react-in-jsx-scope': 'off', // НЕ обязательно: import React from 'react'
		'@typescript-eslint/ban-ts-comment': 'warn', // "@ts-expect-error" - Не дает игнорировать TS
		'@typescript-eslint/explicit-function-return-type': 'off', // Компонент можно сделать стрелочной функцией
		'@typescript-eslint/semi': 'off', // Можно добавлять ";" в конце строки
		'@typescript-eslint/member-delimiter-style': 'off', // Можно добавлять ";" в конце строки (TS: interface)

		'@typescript-eslint/no-non-null-assertion': 'off', // show error with: "!" from "config!.resolve!.modules!.push(paths.src);""
		'@typescript-eslint/no-unnecessary-type-assertion': 'off' // delete: "!" from "config!.resolve!.modules!.push(paths.src);""
	},

	settings: {
		react: {
			version: 'detect'
		}
	},
	globals: {
		__IS_DEV__: true,
		__IS_APP_TYPE__: true
	}

}
